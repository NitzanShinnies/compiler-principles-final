;;; pattern-matcher.scm
;;; The pattern-matching package
;;;
;;; Programmer: Guy Ben-Moshe, 2014


;;;;; Helping defines ;;;;;;;;
(define *void-object* (if #f #f))
(define with (lambda (s f) (apply f s)))

(define is-quote?
	(lambda (e)
		(and (not (null? e))
			 (list? e)
			 (not (null? (car e)))
			 (equal? 'quote (car e))	
	)))

(define let-list?
  (lambda (lst)
    (if (and (list? lst)
             (not (null? lst))
             (andmap (lambda (assignment)
                       (and 
                       		(list? assignment)
                       		(not (< (length assignment) 2))
                            (var? (car assignment))
                            (null? (cddr assignment))))
                     lst))
        #t
        #f)))

(define (flatten lst)
  (cond 
    ((null? lst)
      (list))
    ((list? (car lst))
      (append (flatten (car lst)) (flatten (cdr lst))))
    (else
      (cons (car lst) (flatten (cdr lst))))))

(define *reserved-words*
  '(and begin cond define do else if lambda
    let let* letrec or quasiquote unquote 
    unquote-splicing quote set!))

(define beginify 
  (lambda (l)
    (cond ((null? l) *void-object*) 
          ((null? (cdr l)) (car l)) 
          (else `(begin ,@l))))) 

(define simple-const?
	(lambda(e)
		(or (boolean? e) (char? e) (number? e) (string? e))
		))

(define var?
	(lambda(e)
		(and (not (or (list? e) (pair? e)))
		(not (member e *reserved-words*)))
		))

(define var-list?
	(lambda(e)
		(and (map var? e))
))

(define opt-approve
	(lambda(lst)
		(cond ((eq? lst '()) #f)
			((not (pair? lst))  #t)
			(else (opt-approve (cdr lst))))
	))
(define extract-last
	(lambda(lst)
		(if   (not (pair? lst)) lst (extract-last (cdr lst)))
	))
(define extract-firsts
	(lambda(lst)
		(if   (not (pair? lst)) '() (cons (car lst) (extract-firsts (cdr lst))))
	))

(define quotify
	(lambda (e)
		(if (or (null? e) (symbol? e))
			(list 'quote e)
			e)
	))


;;;;;Mayer stuff;;;;;;

(define ^quote?
  (lambda (tag)
    (lambda (e)
      (and (pair? e)
	   (eq? (car e) tag)
	   (pair? (cdr e))
	   (null? (cddr e))))))

(define unquote? (^quote? 'unquote))
(define unquote-splicing? (^quote? 'unquote-splicing))
(define expand-qq
  (lambda (e)
    (cond ((unquote? e) (cadr e))
	  ((unquote-splicing? e)
	   (error 'expand-qq "unquote-splicing here makes no sense!"))
	  ((pair? e)
	   (let ((a (car e))
		 (b (cdr e)))
	     (cond ((unquote-splicing? a) `(append ,(cadr a) ,(expand-qq b)))
		   ((unquote-splicing? b) `(cons ,(expand-qq a) ,(cadr b)))
		   (else `(cons ,(expand-qq a) ,(expand-qq b))))))
	  ((vector? e) `(list->vector ,(expand-qq (vector->list e))))
	  ((or (null? e) (symbol? e)) `',e)
	  (else e))))

(define Ym
  (lambda fs
    (let ((ms (map
		(lambda (fi)
		  (lambda ms
		    (apply fi (map (lambda (mi)
				     (lambda args
				       (apply (apply mi ms) args))) ms))))
		fs)))
      (apply (car ms) ms))))


(define expand-letrec
  (lambda (letrec-expr)
    (with letrec-expr
      (lambda (_letrec ribs . exprs)
	(let* ((fs (map car ribs))
	       (lambda-exprs (map cdr ribs))
	       (nu (gensym))
	       (nu+fs `(,nu ,@fs))
	       (body-f `(lambda ,nu+fs ,@exprs))
	       (hofs
		(map (lambda (lambda-expr) `(lambda ,nu+fs ,@lambda-expr))
		  lambda-exprs)))
	  `(Ym ,body-f ,@hofs))))))

(define match
  (letrec ((match
	    (lambda (pat e ret-vals ret-fail)
	      (cond ((and (pair? pat) (pair? e))
		     (match (car pat) (car e)
			    (lambda (vals-car)
			      (match (cdr pat) (cdr e)
				     (lambda (vals-cdr)
				       (ret-vals
					(append vals-car vals-cdr)))
				     ret-fail))
			    ret-fail))
		    ((and (vector? pat) (vector? e)
			  (= (vector-length pat) (vector-length e))
			  (match (vector->list pat) (vector->list e)
				 ret-vals ret-fail)))
		    ((procedure? pat)
		     (let ((v (pat e)))
		       (if v (ret-vals v) (ret-fail))))
		    ((equal? pat e) (ret-vals '()))
		    (else (ret-fail))))))
    (lambda (pat e ret-with ret-fail)
      (match pat e
	     (lambda (vals) (apply ret-with vals))
	     ret-fail))))

(define ?
  (lambda (name . guards)
    (let ((guard?
	   (lambda (e)
	     (andmap 
	      (lambda (g?) (g? e))
	      guards))))
      (lambda (value)
	(if (guard? value)
	    (list value)
	    #f)))))

(define _ (? 'dont-care))

;;; composing patterns

(define pattern-rule
  (lambda (pat handler)
    (lambda (e failure)
      (match pat e handler failure))))

(define compose-patterns
  (letrec ((match-nothing
	    (lambda (e failure)
	      (failure)))
	   (loop
	    (lambda (s)
	      (if (null? s)
		  match-nothing
		  (let ((match-rest
			 (loop (cdr s)))
			(match-first (car s)))
		    (lambda (e failure)
		      (match-first e
		       (lambda ()
			 (match-rest e failure)))))))))
    (lambda patterns
      (loop patterns))))

(define split-car-cadr
  (lambda (lst)
    (if (let-list? lst)
        (list (map car lst) (map cadr lst))
        #f)))


;;;;;;; Pattern rules  ;;;;;   

(define parse
  (let ((run
	 (compose-patterns
	  (pattern-rule
	   (? 'c simple-const?)
	   (lambda (c) `(const ,c)))
	  (pattern-rule
	   `(quote ,(? 'c))
	   (lambda (c) `(const ,c)))
	  ;; conditions
	  (pattern-rule
	   `(if ,(? 'test) ,(? 'dit))
	   (lambda (test dit)
	     `(if3 ,(parse test) ,(parse dit) (const ,*void-object*))))
	  (pattern-rule
	   `(if ,(? 'test) ,(? 'dit) ,(? 'dif))
	   (lambda (test dit dif)
	     `(if3 ,(parse test) ,(parse dit) ,(parse dif))))

	  ;; OR expressions - Nitzan


	  ;; normal or
	  (pattern-rule
	 	`(or, (? 'E1) . , (? 'Es))
	 	(lambda (E1 Es) 
	 		`(or , (map parse (cons E1 Es)))))  
	  ;; seriously?	  
	  (pattern-rule
	  	`(or , (? 'E))
	  	(lambda (E) (parse E)))

	  ;; stupid, depricated or
	  (pattern-rule 
	  	`(or)
	  	(lambda () (parse #f)))

	  ;; end OR

	  ;; lambdas from HELL:

	(pattern-rule ;lambda-variadic
		`(lambda ,(? 'var var?) . ,(? 'expr))
		(lambda (var expr)
			`(lambda-variadic ,var ,(parse (beginify expr)))
	 	))

	(pattern-rule ;lambda-opt
		`(lambda ,(? 'var opt-approve) ,(? 'expr) . ,(? 'exprs))
		(lambda (var expr exprs)
			`(lambda-opt ,(extract-firsts var) ,(extract-last var) ,(parse (beginify (cons expr exprs))))
	 	))

	(pattern-rule ;lambda-simple
		`(lambda ,(? 'var var-list?) ,(? 'expr) . ,(? 'exprs))
		(lambda (var expr exprs)
			`(lambda-simple ,var ,(parse (beginify (cons expr exprs))))
	 	))

	  ;; defines

	  (pattern-rule
	  	`(define ,(? 'var var?) ,(? 'expr))
	  	(lambda (var expr)
	  		`(define ,(parse var) ,(parse expr))
	  	))

	  (pattern-rule
	  	`(define ,(? 'var var-list?) . ,(? 'exprs))
	  	(lambda (var exprs)
	  		(parse `(define ,(car var) (lambda ,(cdr var) ,(flatten exprs))))
	  	))

	  ;; begin
	  (pattern-rule
	  	`(begin ,(? 'expr) . ,(? 'exprs))
	  	(lambda (expr exprs)
	  		`(seq ,(map parse (cons expr exprs)))
	  	))

	  ;; lets
	  (pattern-rule
	  	`(let () ,(? 'expr) . ,(? 'exprs list?))
	  	(lambda (expr exprs)
	  		(parse (beginify (cons expr exprs)))
	  	))
       
       (pattern-rule
           `(let ,split-car-cadr ,(? 'expr) . ,(? 'exprs list?))
           (lambda (vars values expr exprs)
             (parse `((lambda ,vars ,expr . ,exprs) . ,values))))

	  (pattern-rule
	   `(let* () ,(? 'expr) . ,(? 'exprs list?))
	   (lambda (expr exprs)
	     (parse (beginify (cons expr exprs)))))
	  (pattern-rule
	   `(let* ((,(? 'var var?) ,(? 'val)) . ,(? 'rest)) . ,(? 'exprs))
	   (lambda (var val rest exprs)
	     (parse `(let ((,var ,val))
		       (let* ,rest . ,exprs)))))

	  ;; and
;	  (pattern-rule
;	  	`(and)
;	  	(lambda () 
;	  		(parse `#t)
;	  	));

;	  (pattern-rule
;	  	`(and ,(? 'expr))
;	  	(lambda (expr)
;	  		(parse expr)
;	  	));

;	  (pattern-rule
;	  	`(and ,(? 'expr) . ,(? 'exprs list?))
;	  	(lambda (expr exprs)
;	  		 (parse `(if ,expr ,(cons 'and exprs)))
;	  	))

          ; and: empty
          (pattern-rule
           `(and)
           (lambda ()
             `,(parse #t)))
          ; and: Single argument
          (pattern-rule
           `(and ,(? 'expr))
           (lambda (expr)
             (parse expr)))
          ; and: Recursive (2 or more arguments) - to if
          (pattern-rule
           `(and ,(? 'expr1) ,(? 'expr2) . ,(? 'exprs list?))
           (lambda (expr1 expr2 exprs)
             (parse `(if ,expr1 (and ,expr2 . ,exprs) #f))))
          

	  ;; cond
	  (pattern-rule
	  	`(cond (else ,(? 'expr)))
	  	(lambda (expr)
	  		(parse expr)
	  	))

	  (pattern-rule
	  	`(cond (,(? 'term) ,(? 'exp)))
	  	(lambda (term exp)
	  		(parse `(if ,term ,(begin exp)))
	  	))

	  (pattern-rule
	  	`(cond (,(? 'term) ,(? 'exp)) . ,(? 'rest))
	  	(lambda (term exp rest)
	  		(if (eq? rest '()) ;if
	  			(parse `(if ,term ,(begin exp))) ;then
	  			(parse `(if ,term ,(begin exp) ,(cons 'cond rest)))) ;else
	  	))

	  ;; Quasimodo
	  (pattern-rule
	 	`(,'quasiquote ,(? 'expr))
	  	(lambda (expr)
	  		(parse (expand-qq expr))
	 	))

	  ;; the letrec of Mayer
	  (pattern-rule
	  	`(letrec . ,(? 'expr))
	  	(lambda (expr)
	  		(parse (expand-letrec (cons 'letrec expr)))
	  	))

	  ;; applic
	  (pattern-rule
	  	`(,(? 'expr) . ,(? 'exprs))
	  	(lambda (expr exprs)
	  		`(applic ,(parse expr) ,(map parse exprs))
	  	))

	  ;; var
	  (pattern-rule
	   (? 'v var?)
	   (lambda (v) `(var ,v)))


	  )))

	  
    (lambda (e)
      (run e
	   (lambda ()
	     (error 'parse
		    (format "I can't recognize this: ~s" e)))))))

;; Assignment 3
;; Programmer: Nitzan Shinnies

;; getters and whatnot - this shit is snatched from PPHELL
(define get-tag
	(lambda (_expr)
		(car _expr)))

(define tagged-by?
	(lambda (_expr _tag)
		(eq? (get-tag _expr) _tag)))

; special cases for is
(define is-lexical-var? 
	(lambda (_expr) 
		(or
			(tagged-by? _expr 'var)
			(tagged-by? _expr 'fvar)
			(tagged-by? _expr 'pvar)
			(tagged-by? _expr 'bvar)
			)))

(define is-lambda?
	(lambda (_expr)
		(or
			(tagged-by? _expr 'lambda-simple)
			(tagged-by? _expr 'lambda-opt)
			(tagged-by? _expr 'lambda-variadic)
			)))

; general is - because I'm a lazy programmer and I hate writing multiple similiar test cases
(define is?
	(lambda (_expr _tag)
		(cond
			; still several special cases :( 
			((eq? _tag 'lexical-var) (is-lexical-var? _expr))
			((eq? _tag 'lambda) (is-lambda? _expr))
			((eq? _tag 'if) (tagged-by? _expr 'if3))
			(else (tagged-by? _expr _tag)))))

(define is-last-element?
	(lambda (_expr) (and (pair? _expr) (null? (cdr _expr)))))

(define list-search 
	(lambda (_list _value _index)
		(cond
			((null? _list) `(,#f))
			((equal? _value (car _list)) `(,#t ,_value, _index))
			(else (list-search (cdr _list) _value (+ _index 1)))
			)))

; searches for variable in the given enviroment, starts from the deepest level
(define search-env
	(lambda (_env _value)
		(if (null? _env) `(,#f,0,0,0)
			(let ((recResult (search-env (cdr _env) _value)))
				(if (eq? #t (car recResult)) recResult
					(let ((selfResult (list-search (car _env) _value 0)))
						(if (eq? #t (car selfResult)) 
							`(, #t, (cadr selfResult), (caddr recResult) , (caddr selfResult))
							`(, #f, 0, (+ 1 (caddr recResult)), 0)
							)))))))


; gets lexical info for value
(define get-lexical-info
	(lambda (_env _params _value)
		(let ((pResult (list-search _params _value 0))) ; first search param list
			(if (eq? #t (car pResult)) 		
				`(pvar ,(cadr pResult), (caddr pResult)) ; return pvar
				(let ((eResult (search-env _env _value))) ; search enviroment
					(if (eq? #t (car eResult))
						`(bvar ,(cadr eResult), (caddr eResult), (cadddr eResult)) ; it's a bvar
						`(fvar , _value)
						))))))

(define pe->lex-pe_actual
	(lambda (_env _params _expr)
		(cond

			((is? _expr 'const) _expr)

			((is? _expr 'var) (get-lexical-info _env _params (cadr _expr)))

			; goddamn lambdas, I hate them!
			((is? _expr 'lambda-simple) `(lambda-simple ,(cadr _expr) ,(pe->lex-pe_actual (append _env (list _params)) (cadr _expr) (caddr _expr))))
			((is? _expr 'lambda-opt) `(lambda-opt ,(cadr _expr) ,(caddr _expr) ,(pe->lex-pe_actual (append _env (list _params)) (append (cadr _expr) (list (caddr _expr))) (cadddr _expr))))
			((is? _expr 'lambda-variadic) `(lambda-variadic ,(cadr _expr),(pe->lex-pe_actual (append _env (list _params)) (list (cadr _expr)) (caddr _expr)))) 

			((is? _expr 'if) 
				`(if3 
					,(pe->lex-pe_actual _env _params (cadr _expr))
					,(pe->lex-pe_actual _env _params (caddr _expr))
					,(pe->lex-pe_actual _env _params (cadddr _expr))
					))

			((is? _expr 'define)
				`(define ,(pe->lex-pe_actual _env _params (cadr _expr)) ,(pe->lex-pe_actual _env _params (caddr _expr))))

			((or (is? _expr 'or) (is? _expr 'seq)) `(,(get-tag _expr) ,(map (lambda (_exp) (pe->lex-pe_actual _env _params _exp)) (cadr _expr))))

			(else 
				(if (is-last-element? _expr) ; if cdr is null, check if expr is null. if it isn't, it's a nested list so check car
					(if (null? _expr) _expr (pe->lex-pe_actual _env _params (car _expr)))
				`(,(get-tag _expr) ,(pe->lex-pe_actual _env _params  (cadr _expr)) , ; cdr isn't null, so assume it's applic or matching form
				(map (lambda (_exp) (pe->lex-pe_actual _env _params _exp)) (caddr _expr)))
				)
			)
		)))	

(define pe->lex-pe
	(lambda (_expr)
		(pe->lex-pe_actual '() `() _expr)))

; algorithm given by Avi in class

; special handler, easier than handling this special case in the special atp
(define atp-seq
	(lambda (_expr _tp?)
		(if (is-last-element? _expr) 
			(list (atp-master (car _expr) _tp?))
			(cons (atp-master (car _expr) #f) (atp-seq (cdr _expr) _tp?))
		)
	))

(define atp-special
	(lambda (_op _expr _tp?)
		(let 
			((_p1 (car _expr))
			 (_p2 (cadr _expr)))
			(cond
				((eq? _op 'if) `(,(atp-master _p1 #f) ,(atp-master _p2 _tp?) ,(atp-master (caddr _expr) _tp?)))
				((or (eq? _op 'lambda-simple) (eq? _op 'lambda-variadic)) `(,_p1 ,(atp-master _p2 #t))) 
				((eq? _op 'lambda-opt) `(,_p1 ,_p2 ,(atp-master (caddr _expr) #t)))
				((eq? _op 'define) `(,_p1 ,(atp-master _p2 _tp?)))
				((eq? _op 'applic) `(,(atp-master _p1 #f) ,(map (lambda (_exp) (atp-master _exp #f)) _p2)))
			)
		)
	))

; ALL HAIL ZOIDBERG!!! the king with the box!

(define atp-master
	(lambda (_expr _tp?)
		(cond
			((or (is? _expr 'const) (is? _expr 'lexical-var)) _expr)
			((or (is? _expr 'or) (is? _expr 'seq)) `(,(get-tag _expr)  ,(atp-seq (cadr _expr) _tp?)))
			((is? _expr 'if) `(if3 ,@(atp-special 'if (cdr _expr) _tp?)))
			((is? _expr 'lambda) `(,(get-tag _expr) ,@(atp-special (get-tag _expr) (cdr _expr) #t)))
			((is? _expr 'applic)
				(if _tp?
					`(tc-applic ,@(atp-special 'applic (cdr _expr) #f))
					`(applic ,@(atp-special 'applic (cdr _expr) #f))
				))
			((is? _expr 'define) `(define ,@(atp-special 'define (cdr _expr) _tp?)))
			(else (error 'ATP-Master (format "\n\n\t\t\t\tALL HAIL ZOIDBERG!\n\n\t\t\t\t  (V) (;,,;) (V)\n\n\t\t\t\tBad parse maybe?\n")))
		)))

(define annotate-tc 
	(lambda (_expr) 
		(atp-master _expr #f)))