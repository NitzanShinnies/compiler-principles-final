CC=gcc
CFLAGS=-g  -c -o
LFLAGS= -o 
IFLAGS=-Iarch

LINKER=gcc

CSRCS=$(src)
COBJS=$(CSRCS:.c=.o)

all: $(COBJS) $(target)

.c.o:
	$(CC) $(IFLAGS) $(CFLAGS) $@ $<

$(target): $(COBJS) 
	$(LINKER) $(LFLAGS) $(target) $(COBJS)

PHONY: clean.

clean:
	rm -f *.o $(target)
