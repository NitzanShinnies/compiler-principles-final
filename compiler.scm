; Compiler Principles - the final solution
; Programmer:
; Nitzan Shinnies
; 2015

(define DEBUG-MODE #t)
; definitions for C file generation 
(define BASE_ADDR 10)
(define BLOCK_SIZE 100)

; -----------------------------------------------------------------------------------------------------
; include files - the code from these files must be copied to the main compiler file before submission!
(load "formatter.scm") ; <-- TODO: check if we can get rid of it
(load "parser-core.scm")
(load "lexical-analyzer.scm")
(load "prim-procs.scm")
(load "label-generator.scm")
(load "constant-table.scm")
(load "free-vars.scm")
(load "error-strings.scm")
(load "code-gen.scm")
; -----------------------------------------------------------------------------------------------------

(define target-output 0);

(define merge-flatten 
	(lambda (_lst)
		(if (null? _lst) `()
			(append (map (lambda (x) x) (car _lst)) (merge-flatten (cdr _lst))))))

(define mMap '(
	(define map
	  ((lambda (y) 
	     ((lambda (map1) 
	   ((lambda (maplist) 
	      (lambda (f . s) 
	        (maplist f s))) 
	    (y (lambda (maplist) 
	         (lambda (f s) 
	      (if (null? (car s)) '() 
	          (cons (apply f (map1 car s)) 
	           (maplist f (map1 cdr s))))))))) 
	      (y (lambda (map1) 
	      (lambda (f s) 
	        (if (null? s) '() 
	       (cons (f (car s)) 
	             (map1 f (cdr s))))))))) 
	   (lambda (f) 
	     ((lambda (x) 
	   (f (lambda (y z)
	        ((x x) y z))))
	      (lambda (x) 
	   (f (lambda (y z)
	        ((x x) y z))))))))))
(define Yh 
	'(
	(define Ym
	  (lambda fs
	    (let ((ms (map
	      (lambda (fi)
	        (lambda ms
	          (apply fi (map (lambda (mi)
	                 (lambda args
	                   (apply (apply mi ms) args))) ms))))
	      fs)))
	      (apply (car ms) ms))))))


(define compile-scheme-file
	(lambda (_sourcefile _targetfile)
		(begin
			(set! BASE_ADDR (get-aligned-block-size (car err-strings)))
			(let*
				(			
					(_input_expr_list (file->sexprs _sourcefile))
					(_expr_list (append mMap Yh _input_expr_list))
					(_parsed_exprs (map parse (reverse _expr_list)))
					(_parsed_annotated-lexed_exprs (map annotate-tc (map pe->lex-pe (reverse _parsed_exprs))))
					(_primitive-const-list (merge-flatten (map get-const-list _parsed_annotated-lexed_exprs))) ; UNIFY
					(_topological-sorted-const-list (apply append (map const-topological-sort _primitive-const-list))) ; UNIFY
					(_const-set (reverse (list->set (reverse _topological-sorted-const-list))))
					(_extended-const-list (make-extended-const-list _const-set BASE_ADDR))
					(_primitive-fvar-list (merge-flatten (map get-fvar-list _parsed_annotated-lexed_exprs)))
					(_fvar-set (list->set _primitive-fvar-list))
					(_extended-fvar-list (make-fvar-extended-list _fvar-set))
					(_header (make-program-header _extended-const-list _extended-fvar-list))
					(_footer make-program-footer)
					(_output (open-output-file _targetfile '(replace)))
				)

				(begin
					; stuff goes here
					(set! target-output _output)
					(display _header _output)
					(display "\n" _output)
					(generate-code (reverse _parsed_annotated-lexed_exprs) _extended-const-list _extended-fvar-list)
					(display "\n" _output)
					(display _footer _output)
					(close-output-port _output)
					(if DEBUG-MODE
						(begin (let ((_debug (open-output-file "debug.txt" '(replace))))
								(display 
									(string-append
										"Compilation report for file: " 
										_sourcefile
										"\n"
										"Compiled at "
										(format "~s\n" (current-date))
										"\n\t" (make-string 30 #\#) " Constant Table " (make-string 30 #\#) "\n\n"
										(apply string-append (map (lambda (_expr) (format "~s\n" _expr)) _extended-const-list))
										"\n\t" (make-string 30 #\#) " Freevar List " (make-string 30 #\#) "\n\n"
										(apply string-append (map (lambda (_expr) (format "~s\n" _expr)) (reverse _extended-fvar-list)))
										"\n\t" (make-string 30 #\#) " Resulting Parse Tree " (make-string 30 #\#) "\n\n"
									)
									_debug)
								(pretty-print _parsed_annotated-lexed_exprs _debug)					 	
						 		(close-output-port _debug)
						 		))
					)
					; clean RAM
					(set! extended-fvar-lst `()) 
					(set! ext-const-lst `())
					(set! private-const `())
				)
			)
		)
	)
)

(define make-includes 
	(lambda (_include-exp _includes-lst)
		(apply string-append (map (lambda (_filename) (formatter _include-exp `((filename ,_filename)))) _includes-lst))))


(define make-primitive-scm-procs-code
	(lambda (_prim-procs-lst)
		(apply string-append (map (lambda (_proc) (string-append (eval (cadr _proc)) "\n")) _prim-procs-lst))))

>(define update-primitive-procs-offset
 	(lambda (_proc-lst _base-offset) 
 		(begin 
 			(set-car! (car _proc-lst) _base-offset) 
 			(if (not (null? (cdr _proc-lst))) (update-primitive-procs-offset (cdr _proc-lst) (+ _base-offset 3)) `())
 		)))

(define make-primitive-scm-procs-table ; TODO: needs to update last-allocated-addr
	(lambda (_prim-procs-lst _base-offset )
		(if (null? _prim-procs-lst) "\n\t\t/* no primitive procs required by given Scheme file! /* \n"
			(begin
				(update-primitive-procs-offset _prim-procs-lst _base-offset)
				(apply string-append (map (lambda (_proc) (string-append 
					"\n\t\t/* " (symbol->string (cadr _proc)) " */\n\n"
					"\t\t\tMOV(IND(" (format "~s" (car _proc)) "), IMM(T_CLOSURE));\n"
					"\t\t\tMOV(IND(" (format "~s" (+ (car _proc) 1)) "), IMM(666));\n"
					"\t\t\tMOV(IND(" (format "~s" (+ (car _proc) 2)) "), LABEL(" (cadddr _proc) "));\n"
					"\n\t\t\t#define " (caddr _proc) " " (format "~s" (car _proc)) "\n"
					"\n\t\t/* ---------------------- */\n"
				)) _prim-procs-lst)))
		)))

(define get-primitive-procs-table-size
	(* 3 (length prim-scm-proc-master-list)))

(define get-aligned-block-size 
	(lambda (_size)
		(* (+ 1 (floor (/ _size BLOCK_SIZE))) BLOCK_SIZE)))

(define make-program-header 
	(lambda (_extended-const-list _extended-fvar-list)
		(let* 
			(
				(_std-includes `("stdio.h" "stdlib.h"))
				(_cisc-includes (map (lambda (_name) (string-append "\\\\\\\"" _name "\\\\\\\"")) `("cisc.h" "debug_macros.h")))
				(_lib-includes (map (lambda (_name) (string-append "\\\\\\\"" _name "\\\\\\\"")) `("char.lib" "io.lib" "math.lib" "scheme.lib" "string.lib" "system.lib")))
				(_defines `(,(if DEBUG-MODE "DO_SHOW\t1" "DO_SHOW\t0") "SCM_ARG(n)\tFPARG(n+2)" "SCM_ENV\tFPARG(0)" "SCM_ARGC\tFPARG(1)"))
				(_cisc-const-table (make-cisc-const-table _extended-const-list))
				(_cisc-const-macros (make-cisc-const-macros _extended-const-list))
				(_strings-linked-lst-obj (make-strings-linked-list))
				(_strings-linked-lst-anchor (car _strings-linked-lst-obj))
				(_strings-linked-lst (cdr _strings-linked-lst-obj))
				(_prim-proc-lst (reverse (list->set (reverse (make-fvar-prim-procs-list prim-scm-proc-master-list _extended-fvar-list)))))
			)	

			(formatter 
				(string-append
					"/*\nCompiler Principles - final solution\nNitzan Shinnies & Guy Ben-Moshe AKA GBM\n(C) 2015 \n*/\n"
					"\n/* STDLIB includes */\n"
					(make-includes "#include <~{filename}>\n" _std-includes)
					"\n/* macro definitions */\n"
					(make-includes "#define ~{filename}\n" _defines)
					"\n/* CISC includes */\n"
					(make-includes "#include ~{filename}\n" _cisc-includes)
					; int main() starts here
					"\nint main() {\n\n\tSTART_MACHINE; \n\tJUMP(CONTINUE);\n"
					; wierd include jump over type thingy
 					"\n\t/* LIB includes */\n";
					(make-includes "#include ~{filename}\n" _lib-includes)
					"\n\t/* constant macros */\n"
					_cisc-const-macros
					"\n\t/* strings linked list anchor*/\n"
					_strings-linked-lst-anchor
					; inject primitive procs
					"\n\t/* primitive procedures*/\n"
					(make-primitive-scm-procs-code _prim-proc-lst)		
					"\n\tCONTINUE:\n"
					; basic constant table - change later
					"\n\t\t/* set RODATA segment size */\n"
					"\t\tADD(IND(0), IMM(" (number->string (get-aligned-block-size (+ last-allocated-addr (* 3 (length _prim-proc-lst))))) "));\n"
					"\n\t\t/* Error strings - C STRINGS, NOT SCHEME STRINGS! */\n"
					(cdr err-strings)
					"\n\t\t/* constant table */\n"
					_cisc-const-table
					"\n\t\t/* string pointers linked list */\n"
					_strings-linked-lst
					"\n\t\t/* primitive procs table */\n"
					(make-primitive-scm-procs-table _prim-proc-lst (+ last-allocated-addr 1))
					"\n\t\t/* freevar pointers init values */\n"
					(make-fvar-init-code _extended-fvar-list) ; must come after prim procs init!

					"\n\t\tMOV(R0,IMM(T_NIL)); /* set R0 to SCM_NULL prior to executing generated code. */\n"
					"\n\t/* ################## COMPILER GENERATED CODE STARTS HERE ################## */\n\n"			

				)
			)
		)
	)
)

(define make-program-footer 
	(formatter
		(string-append
			"\n\t/* ################## GENERATED CODE ENDS HERE ################## */\n\n"
			"\tSTOP_AND_EXIT:\n"
			"\tSTOP_MACHINE;\n"
			"\n\treturn 0;\n}"
		)
	)
)

(define file->sexprs
	(lambda (_sourcefile)
		(let ((_source (open-input-file _sourcefile)))
			(letrec ((run
				(lambda ()
					(let ((_expr (read _source)))
						(if (eof-object? _expr)
							(begin (close-input-port _source) '())
							(cons _expr (run)))))))
			(run)))))

(define generate-code
	(lambda (_exprs-lst _ext-const-lst _ext-fvar-lst)
		(begin 
			(set! extended-fvar-lst _ext-fvar-lst) ; override const and fvars list to ensure data integrity
			(set! ext-const-lst _ext-const-lst)	
			(reset-label-count)
			(map (lambda (_expr) (begin (code-gen _expr 0 0 2) (print-R0))) _exprs-lst)		
	)))

(define print-R0
	(lambda ()
		(display
			(string-append
			"\n\t\tBEGIN_LOCAL_LABELS CONT;\n" 
			"\t\tCMP(R0,IMM(SOB_VOID));\n"
			"\t\tJUMP_EQ(CONT);\n"
			"\t\tPUSH(R0);\n"
			"\t\tCALL(WRITE_SOB);\n"
			"\t\tCALL(NEWLINE);\n"
			"\t\tPOP(R0);\n"
			"\t\tCONT:\n"
			"\t\tNOP;\n"
			"\t\tEND_LOCAL_LABELS\n"
			)
			target-output
	)))
