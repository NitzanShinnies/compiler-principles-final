(load "pc.scm")

(define <digit-0-9>
  (range #\0 #\9))

(define <digit-1-9>
  (range #\1 #\9))

(define sexpr->string
          (lambda (e)
            (format "~a" e)))

(define unicode-char-overflow-right (integer->char 9758))
(define unicode-char-overflow-left (integer->char 9756))
(define unicode-char-double-overflow (integer->char 9757))

(define <whitespace> (const (lambda (_char) (char-ci<=? _char #\space))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ALIGN LOGIC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define push-logic ; adds spaces to str, which most of times will be just ""
  (lambda (num str)
    (if (< num 1) 
      str
      (push-logic (- num 1) (string-append str " ")))
))

(define left-logic ; gets a number and a string, cut the string if needed or adds spaces to it from the right side.
  (lambda (num)
    (lambda (str)
    (let ((length (string-length str)))
      (if (> length num) 
        (string-append (substring str 0 (- num 1)) (sexpr->string unicode-char-overflow-right))
        (string-append str (push-logic  (- num length) "")))
    ))))

(define right-logic ; gets a number and a string, cut the string if needed or adds spaces to it from the left side.
  (lambda(num)
    (lambda (str)
    (let ((length (string-length str)))
      (if (> length num) 
        (string-append (sexpr->string unicode-char-overflow-left) (substring str (- length num -1) length))
        (string-append (push-logic (- num length) "") str))
    ))))

(define center-cut ; cuts the string from right and left recursively
  (lambda(num str right?)
    (let ((length (string-length str)))
    (if(> num 0) 
      (center-cut (- num 1)  (if right? (substring str 0 (- length 1)) (substring str 1 length)) (not right?))
      str
      )
    )))

(define center-push
  (lambda(num str)
    (let* ((length (string-length str))
      (difference (- num length))
      (of (if (even? difference) 0 1)))
        (string-append (push-logic (/ difference 2) "") str (push-logic (+ (/ difference 2) of) ""))
  )))

(define center-logic
  (lambda (num)
    (lambda (str)
    (let ((length (string-length str)))
      (cond ((= num 1) (if (= length 1) str (sexpr->string unicode-char-double-overflow)))
            ((= num length) str)
            ((> length num) (string-append (sexpr->string unicode-char-overflow-left) (center-cut (+ (- length num) 2) str #t) (sexpr->string unicode-char-overflow-right)))
            (else (center-push num str)))
      ))
    ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define <natural-number>
  (new (*parser (char #\0))
       (*pack (lambda (_) 0))
       (*parser <digit-1-9>)
       (*parser <digit-0-9>) *star
       (*caten 2)
       (*pack-with
	(lambda (a s)
	  (string->number
	   (list->string
	    `(,a ,@s)))))

       (*disj 2)
       done))

(define <symbol>
  (new (*parser (range-ci #\a #\z))
       (*parser (range-ci #\0 #\9))
       (*parser (one-of "!$^*-_=+<>?/"))
       (*disj 3) *plus
       (*pack
	(lambda (s)
	  (string->symbol
	   (list->string s))))
       (*parser <natural-number>)
       *diff
       done))

(define <align-symbol>
  (new
    (*parser (char #\{))
    (*parser <symbol>)
    (*parser (char #\}))
    (*caten 3)
    (*pack-with
      (lambda (c1 width c2) (
        lambda (_values)
          (let ((_result (assoc width _values)))
            (if (pair? _result)
             (cadr _result)
              1
            ))
          ))
       )
    done))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ARROWS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define <center-arrow>
  (new
    (*parser (char #\<))
    (*parser (char #\-)) *plus
    (*parser <align-symbol>)
    (*parser <natural-number>)
    (*disj 2)
    (*parser (char #\-)) *plus
    (*parser (char #\>))
    (*caten 5)
    (*pack-with
      (lambda (c1 c2 width c3 c4) (list center-logic width)))
    done))

(define <left-arrow>
  (new
    (*parser (char #\<))
    (*parser (char #\-)) *plus
    (*parser <align-symbol>)
    (*parser <natural-number>)
    (*disj 2)
    (*parser (char #\-)) *plus
    (*caten 4)
    (*pack-with
      (lambda (c1 c2 width c3) (list left-logic width)))
    done))

;define dict (apply append dicts)
;(if (assoc variable dict)   (car (cdr (assoc variable dict)))

(define <right-arrow>
  (new
    (*parser (char #\-)) *plus
    (*parser <align-symbol>)
    (*parser <natural-number>)
    (*disj 2)
    (*parser (char #\-)) *plus
    (*parser (char #\>))
    (*caten 4)
    (*pack-with
      (lambda ( c1 width c2 c3) (list right-logic width)))
    done))

(define <arrows>
  (new
    (*parser <center-arrow>)
    (*parser <left-arrow>)
    (*parser <right-arrow>)
    (*disj 3)
    (*pack (lambda (arrow) arrow)) ; a list of width, symbol and direction.
    done))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define <string-field>
  (new 
  (*parser (char #\~))
  (*parser <arrows>)*star
  (*parser (char #\{))
  (*parser <whitespace>) *star
  (*parser <symbol>)
  (*parser <whitespace>) *star
  (*parser (char #\}))
  (*caten 7)
  (*pack-with
    (lambda (c1 _align-info c2 c3 _symbol c4 c5)
      (if (> (length _align-info) 0) (list (car _align-info) (cdr _align-info) _symbol) _symbol)))
  done))


(define <text-char>
  (new (*parser (range-ci #\a #\z))
       (*parser (range-ci #\0 #\9))
       (*parser (one-of " !$^*-_=+<>?/[()],.@#$%^&*"))
       (*parser (char #\newline))
       (*parser (word-ci "~~"))
       (*disj 5) *plus
       (*pack (lambda (s) s))
       done))

(define <meta-char-special-as-text>
  (new
    (*parser (word-ci "\\{"))
    (*parser <text-char>) *star
    (*parser (char #\}))
    (*caten 3)
    (*pack-with (lambda (_open _text _close) (list _open _text _close)))
    done))

(define <meta-char-normal-as-text>
  (new
    (*parser (word-ci "\\"))
    (*parser (one-of "\\tnrf{}"))
    (*caten 2)
    (*pack-with (lambda (_prefix _symbol) (list _prefix _symbol)))
    done))

(define <meta-char-as-text>
  (new
    (*parser <meta-char-special-as-text>)
    (*parser <meta-char-normal-as-text>)
    (*disj 2)
    (*pack (lambda (_char) _char))
    done))


(define <no-field-input-string>
  (new
    (*parser <text-char>)
    (*parser <meta-char-as-text>) 
    (*disj 2)
    (*pack (lambda (t1)  (flatten-replace t1 '())))
    done))

(define <string-with-field>
  (new 
    (*parser <no-field-input-string>) *star
    (*parser <string-field>)
    (*parser <no-field-input-string>) *star
    (*caten 3)
    (*pack-with (lambda (_header _symbol _footer) (list 
        ;(list->string (cond ((pair? _header) (car _header)) (else _header)))
        _header
        (lambda (_values) 
          (let 
            (
            (_result (assoc (if (list? _symbol) (caddr _symbol) _symbol) _values))
            (_align-func (if (list? _symbol) (caar _symbol) (lambda (_t1) (lambda (_t2) _t2))))
            (_align-width  (if 
                            (list? _symbol) 
                            (if (procedure? (cadar _symbol))
                              ((cadar _symbol) _values)
                              (cadar _symbol)) 
                            1)
              )
            )
            (if (pair? _result) 
              (let ((_value (cadr _result)))
                (string->list ((_align-func (if (> _align-width 0) _align-width 1)) (if (string? _value) _value (format "~s" _value)))))
               '()
        )))
        _footer
        )
    ))
   *star
    done))

(define <string-field-as-text>
  (new 
  (*parser (char #\~))
  (*parser <arrows>)*star
  (*parser (char #\{))
  (*parser <whitespace>) *star
  (*parser <text-char>)
  (*parser <whitespace>) *star
  (*parser (char #\}))
  (*caten 7)
  (*pack-with
    (lambda (c1 c2 c3 sym c4 c5 c6) (list c1 c2 c3 sym c4 c5 c6))
    )
  done))

(define <string-with-field-as-text>
  (new 
    (*parser <text-char>) *star
    (*parser <string-field-as-text>)
    (*parser <text-char>) *star
    (*caten 3)
    (*pack-with (lambda (_header _symbol _footer) (list _header _symbol _footer)))
   *star
    done))

 (define flatten-replace
  (lambda (_list _values)
    (cond ((null? _list) '())
          ((not (pair? _list)) (list 
            (if (procedure? _list) (_list _values) _list)
            ))
          (else (append (flatten-replace (car _list) _values)
                        (flatten-replace (cdr _list) _values))))))

(define <hex-digit>
  (let ((zero (char->integer #\0))
  (lc-a (char->integer #\a))
  (uc-a (char->integer #\A)))
    (new (*parser (range #\0 #\9))
   (*pack
    (lambda (ch)
      (- (char->integer ch) zero)))

   (*parser (range #\a #\f))
   (*pack
    (lambda (ch)
      (+ 10 (- (char->integer ch) lc-a))))

   (*parser (range #\A #\F))
   (*pack
    (lambda (ch)
      (+ 10 (- (char->integer ch) uc-a))))

   (*disj 3)
   done)))

(define <XX>
  (new (*parser <hex-digit>)
       (*parser <hex-digit>)
       (*caten 2)
       (*pack-with
  (lambda (h l)
    (+ l (* h 16))))
       done))

(define <XXXX>
  (new (*parser <XX>)
       (*parser <XX>)
       (*caten 2)
       (*pack-with
  (lambda (h l)
    (+ l (* 256 h))))
       done))

(define <XXXXXXXX>
  (new (*parser <XXXX>)
       (*parser <XXXX>)
       (*caten 2)
       (*pack-with
  (lambda (h l)
    (+ l (* 1024 h))))
       done))

(define <hex-char>
  (new 
       (*parser (word-ci "\\{0x"))
       (*parser <XXXXXXXX>)
       (*parser <XXXX>)
       (*parser <XX>)
       (*disj 3)
       (*pack integer->char)
       (*parser (char #\}))
       (*caten 3)
       (*pack-with (lambda (_< ch _>) ch))
       done))

; because of "#\}" it's easier to just write another one
(define <hex-char-special>
  (new
       (*parser (word-ci "\\u"))
       (*parser (word-ci "\\x"))
       (*parser (word-ci "\\h"))
       (*parser (word-ci "\\16#"))
       (*disj 4)
       (*parser <XXXX>)
       (*parser <XX>)
       (*disj 2)
       (*pack integer->char)
       (*caten 2)
       (*pack-with (lambda (_< ch) ch))
       done))

(define ^<meta-char>
  (lambda (str ch)
    (new (*parser (word str))
   (*pack (lambda (_) ch))
   done)))

(define <string-meta-char>
  (new (*parser <hex-char>)
       (*parser <hex-char-special>)
       (*parser (^<meta-char> "\\\\" #\\))
       (*parser (^<meta-char> "\\\"" #\"))
       (*parser (^<meta-char> "\\n" #\newline))
       (*parser (^<meta-char> "\\r" #\return))
       (*parser (^<meta-char> "\\t" #\tab))
       (*parser (^<meta-char> "\\f" #\page)) ; formfeed
       (*parser (^<meta-char> "\\{lambda}" (integer->char 955)))
       (*parser (^<meta-char> "\\{alef}" (integer->char 1488)))
       (*parser (^<meta-char> "\\{bismillah}" (integer->char 65021)))
       (*parser (^<meta-char> "\\{smiley}" (integer->char 9786)))
       (*parser (^<meta-char> "\\{tab}" #\tab))
       (*parser (^<meta-char> "\\{newline}" #\newline))
       (*parser (^<meta-char> "\\{return}" #\return))
       (*parser (^<meta-char> "\\{page}" #\page))
       (*parser (^<meta-char> "\\{" #\{))
       (*parser (^<meta-char> "\\}" #\}))       
       (*parser (^<meta-char> "~~" #\~))
       (*disj 19)
       done))

(define <string-char>
  (new (*parser <string-meta-char>)

       (*parser <any-char>)

       (*parser (char #\"))
       (*parser (char #\\))
       (*disj 2)

       *diff
       (*disj 2)
       done))

(define <string>
  (new 
       (*parser <string-char>) *star
       
       (*pack
  (lambda (chars)
    (list->string chars)))

       done))

(define succ
 (lambda (e s) (format "~a" e)))

(define fail
 (lambda (w) (format "FAIL!!! ~a" w)))

(define meta-processor
 (lambda (_procstr _unprocstr)
  (let ((_result 
    (string-append 
      (if (string? _procstr) _procstr "")
      (if (list? _unprocstr) (list->string _unprocstr) "")
      ))) (<string> (string->list _result) (lambda (e s) (succ e s)) (lambda (w) (fail w)))
      )))
    
(define <full-input-string>
  (new
    (*parser <text-char>)
    (*parser <string-field-as-text>) 
    (*parser <meta-char-as-text>) 
    (*disj 3)
    (*pack (lambda (t1) (list t1)))
    done))

(define <comment>
  (new
    (*parser <full-input-string>) *star
    (*parser (word-ci "~{{"))
    (*parser <full-input-string>) *star
    (*delayed (lambda () <comment>))
    (*parser (word-ci "}}"))
    (*parser <full-input-string>) *star
    (*caten 6)
    (*pack-with (lambda (_header _t1 _comment-text _rec-result _t2 _footer) (list _header _footer)))
    *star
    done))


(define strip-comments
  (lambda (_string _values)
  (<comment> 
    (string->list _string)  
    (lambda (e s) (process-fields (string-append (list->string (flatten-replace e _values)) (list->string s)) _values))       
    (lambda (w) (fail w)))))

(define (formatter . args)
  (strip-comments (car args) (if (= (length args) 2) (cadr args) '())))

(define process-fields (lambda (_string _values)
  (<string-with-field> 
    (string->list _string) 
    (lambda (e s) (meta-processor 
      (string-append (list->string (flatten-replace (flatten-replace e _values) '())))
       s
       )) 
    (lambda (w) (fail w)))))

