(define base-err-lst
	`(
		("RUNTIME ERROR: Unspecified" "ERR_GENERIC")
		("RUNTIME ERROR: Invalid APPLIC - object is not a closure." "ERR_NOT_A_CLOSURE")
		("RUNTIME ERROR: Compilation error encountered during runtime." "ERR_COMPILE_ERROR")
	))

(define err-search-lst
	'(
		(generic . "ERR_GENERIC")
		(closure . "ERR_NOT_A_CLOSURE")
		(compile . "ERR_COMPILE_ERROR")
		))

(define make-error-strings
	(lambda (_base-addr)
		(let* ((_err-str-lst (append (map (lambda (_str) `(,0 ,(cdr _str) ,(append (map char->integer (string->list (car _str))) (list 0)))) base-err-lst))))			
		(begin
			(set! _offset _base-addr)
			(map (lambda (_err-str) (begin (set-car! _err-str _offset) (set! _offset (+ _offset (length (caddr _err-str)))))) (reverse _err-str-lst))
			(cons _offset 
				(apply string-append 
					(map (lambda (_err-str) 
						(string-append
							"\t\t#define "
							(caadr _err-str)
							" "
							(number->string (car _err-str))
							"\n\n"
							(write-cisc-mem-vals (car _err-str) (reverse (caddr _err-str)))
							"\n"
						))
						_err-str-lst)
					)
				)
		))))
;			(string-append (map (lambda (_err-str) 

(define write-cisc-mem-vals
	(lambda (_base-addr _val-list)
		(let ((_offset (- _base-addr 1)))
			(apply string-append 
				(reverse (map (lambda (_val) 
					(begin 
						(set! _offset (+ _offset 1)) 
						(string-append 
							"\t\tMOV(IND(" 
							(number->string _offset) 
							"),IMM(" 
							(number->string _val) 
							"));\n"
						)))
					_val-list))))))

; declare make-error-strings to make global ref.
; this will be a global since we need err string size for BASE_ADDR,
; and it will also be used in make-header.
(define err-strings (make-error-strings BASE_ADDR))

