/*
Compiler Principles - final solution
Nitzan Shinnies & Guy Ben-Moshe AKA GBM
(C) 2015 
*/

/* STDLIB includes */
#include <stdio.h>
#include <stdlib.h>

/* macro definitions */
#define DO_SHOW	1
#define SOB_VOID	1
#define SOB_NIL	2
#define SOB_FALSE	3
#define SOB_TRUE	5
#define SCM_ARG(n)	FPARG(n+2)
#define SCM_ENV	FPARG(0)
#define SCM_ARGC	FPARG(1)

/* CISC includes */
#include "cisc.h"
#include "debug_macros.h"

int main() {

	START_MACHINE; 
	JUMP(CONTINUE);

/* LIB includes */
#include "char.lib"
#include "io.lib"
#include "math.lib"
#include "scheme.lib"
#include "string.lib"
#include "system.lib"

/* primitive procedures */
	PRIM_SCM_PROC_EQUIV:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(R1);
		PUSH(R2);
		PUSH(R3);
		PUSH(R4);
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,1); /* init counter */
		MOV(R4,SCM_ARG(0)); /* move first value PTR to test-again register */
		MOV(R4,INDD(R4,1)); /* get actual value */
		BEGIN_LOCAL_LABELS CMP_LOOP, EQ_FAIL, EQ_EXIT;
		CMP_LOOP:
			MOV(R3,R4); /* move old test value to test register */
			MOV(R4,SCM_ARG(R2)); /* get new comparison value PTR */
			MOV(R4,INDD(R4,1));
			CMP(R3,R4);
			JUMP_NE(EQ_FAIL); /* isn't in relation, fail */
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CMP_LOOP); /* more args, loop */
		PUSH(IMM(1)); /* in relation, set result to true and goto exit */
		JUMP(EQ_EXIT);
		EQ_FAIL:
		PUSH(IMM(0));
		EQ_EXIT:
		/* make SOB_BOOL */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(R4);
		POP(R3);
		POP(R2);
		POP(R1);
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_GT:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(R1);
		PUSH(R2);
		PUSH(R3);
		PUSH(R4);
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,1); /* init counter */
		MOV(R4,SCM_ARG(0)); /* move first value PTR to test-again register */
		MOV(R4,INDD(R4,1)); /* get actual value */
		BEGIN_LOCAL_LABELS CMP_LOOP, EQ_FAIL, EQ_EXIT;
		CMP_LOOP:
			MOV(R3,R4); /* move old test value to test register */
			MOV(R4,SCM_ARG(R2)); /* get new comparison value PTR */
			MOV(R4,INDD(R4,1));
			CMP(R3,R4);
			JUMP_LE(EQ_FAIL); /* isn't in relation, fail */
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CMP_LOOP); /* more args, loop */
		PUSH(IMM(1)); /* in relation, set result to true and goto exit */
		JUMP(EQ_EXIT);
		EQ_FAIL:
		PUSH(IMM(0));
		EQ_EXIT:
		/* make SOB_BOOL */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(R4);
		POP(R3);
		POP(R2);
		POP(R1);
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_LT:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(R1);
		PUSH(R2);
		PUSH(R3);
		PUSH(R4);
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,1); /* init counter */
		MOV(R4,SCM_ARG(0)); /* move first value PTR to test-again register */
		MOV(R4,INDD(R4,1)); /* get actual value */
		BEGIN_LOCAL_LABELS CMP_LOOP, EQ_FAIL, EQ_EXIT;
		CMP_LOOP:
			MOV(R3,R4); /* move old test value to test register */
			MOV(R4,SCM_ARG(R2)); /* get new comparison value PTR */
			MOV(R4,INDD(R4,1));
			CMP(R3,R4);
			JUMP_GT(EQ_FAIL); /* isn't in relation, fail */
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CMP_LOOP); /* more args, loop */
		PUSH(IMM(1)); /* in relation, set result to true and goto exit */
		JUMP(EQ_EXIT);
		EQ_FAIL:
		PUSH(IMM(0));
		EQ_EXIT:
		/* make SOB_BOOL */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(R4);
		POP(R3);
		POP(R2);
		POP(R1);
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_ADD:
		/* backup fp and used registers */
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,IMM(1)); /* init counter */
		MOV(R3,SCM_ARG(0)); /* init result register */
		MOV(R3,INDD(R3,1));
		BEGIN_LOCAL_LABELS CALC_LOOP;
		CALC_LOOP:
			MOV(R4,SCM_ARG(R2)); /* move arg PTR to register */
			MOV(R4,INDD(R4,1)); /* get actual value */
			ADD(R3,R4);
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CALC_LOOP); /* more args, loop */
		/* make SOB_INTEGER from result */
		PUSH(R3);
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_SUB:
		/* backup fp and used registers */
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,IMM(1)); /* init counter */
		MOV(R3,SCM_ARG(0)); /* init result register */
		MOV(R3,INDD(R3,1));
		BEGIN_LOCAL_LABELS CALC_LOOP;
		CALC_LOOP:
			MOV(R4,SCM_ARG(R2)); /* move arg PTR to register */
			MOV(R4,INDD(R4,1)); /* get actual value */
			SUB(R3,R4);
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CALC_LOOP); /* more args, loop */
		/* make SOB_INTEGER from result */
		PUSH(R3);
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_MUL:
		/* backup fp and used registers */
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,IMM(1)); /* init counter */
		MOV(R3,SCM_ARG(0)); /* init result register */
		MOV(R3,INDD(R3,1));
		BEGIN_LOCAL_LABELS CALC_LOOP;
		CALC_LOOP:
			MOV(R4,SCM_ARG(R2)); /* move arg PTR to register */
			MOV(R4,INDD(R4,1)); /* get actual value */
			MUL(R3,R4);
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CALC_LOOP); /* more args, loop */
		/* make SOB_INTEGER from result */
		PUSH(R3);
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_DIV:
		/* backup fp and used registers */
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARGC); /* move ARGC to R1 */
		MOV(R2,IMM(1)); /* init counter */
		MOV(R3,SCM_ARG(0)); /* init result register */
		MOV(R3,INDD(R3,1));
		BEGIN_LOCAL_LABELS CALC_LOOP;
		CALC_LOOP:
			MOV(R4,SCM_ARG(R2)); /* move arg PTR to register */
			MOV(R4,INDD(R4,1)); /* get actual value */
			DIV(R3,R4);
			INCR(R2);
			CMP(R2,R1);
		JUMP_LT(CALC_LOOP); /* more args, loop */
		/* make SOB_INTEGER from result */
		PUSH(R3);
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_CHAR:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_CHAR);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_INTEGER:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_INTEGER);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_NULL:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_NIL);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_PAIR:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_PAIR);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_PROCEDURE:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_CLOSURE);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_STRING:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_STRING);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_SYMBOL:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_SYMBOL);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_VECTOR:
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R0),T_VECTOR);
		JUMP_NE(FAIL);
		PUSH(1);
		JUMP(EXIT);
		FAIL:
		PUSH(0);
		EXIT:
		/* make boolean result value */
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_ZERO:
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R1),T_INTEGER);
		JUMP_NE(FAIL);
		CMP(INDD(R1,1),IMM(0));
		JUMP_NE(FAIL);
		PUSH(IMM(1));
		JUMP(EXIT);
		FAIL:
		PUSH(IMM(0));
		EXIT:
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POPAD;
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_IS_NUMBER:
		JUMP(PRIM_SCM_PROC_IS_INTEGER);

	PRIM_SCM_PROC_IS_EQ:
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		/* get args from stack */
		MOV(R1,SCM_ARG(0));
		MOV(R2,SCM_ARG(1));
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(IND(R1),IND(R2));
		JUMP_NE(FAIL);
		CMP(INDD(R1,1),INDD(R2,1));
		JUMP_NE(FAIL);
		PUSH(IMM(1));
		JUMP(EXIT);
		FAIL:
		PUSH(IMM(0));
		EXIT:
		CALL(MAKE_SOB_BOOL);
		DROP(1);
		POPAD;
		POP(FP);
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_MAKE_STRING:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move replicate obj count to R1 */
		MOV(R2, SCM_ARG(1)); /* move replicate obj ptr to R2 */
		MOV(R3, IMM(0)); /* init counter */
		BEGIN_LOCAL_LABELS PUSH_LOOP;
		PUSH_LOOP:
		PUSH(R2);
		INCR(R3);
		CMP(R3,R1);
		JUMP_LT(PUSH_LOOP);
		PUSH(R1) /* push char count */
		/* make SOB_STRING*/
		CALL(MAKE_SOB_STRING);
		DROP(R1+1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_STRING_TO_SYMBOL:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move original ptr R1 */
		MOV(R2, INDD(R1,1)); /* move original str length to R2 */
		MOV(R3, R2); /* init counter */
		ADD(R3, 3); /* make room for identifier, strlen and null terminator */
		PUSH(R3);
		CALL(MALLOC);
		DROP(1);
		MOV(IND(R0),T_SYMBOL);
		MOV(INDD(R0,1),R2);
		MOV(R3,0);
		BEGIN_LOCAL_LABELS CLONE_LOOP;
		CLONE_LOOP:
		MOV(INDD(R0,R3+2),INDD(R1,R3+2));
		INCR(R3);
		CMP(R3,R2);
		JUMP_LT(CLONE_LOOP);
		/* done copying, push null str terminator and cleanup */
		MOV(INDD(R0,R3+3),IMM(0));
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_SYMBOL_TO_STRING:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move original ptr R1 */
		MOV(R2, INDD(R1,1)); /* move original str length to R2 */
		MOV(R3, R2); /* init counter */
		ADD(R3, 3); /* make room for identifier, strlen and null terminator */
		PUSH(R3);
		CALL(MALLOC);
		DROP(1);
		MOV(IND(R0),T_STRING);
		MOV(INDD(R0,1),R2);
		MOV(R3,0);
		BEGIN_LOCAL_LABELS CLONE_LOOP;
		CLONE_LOOP:
		MOV(INDD(R0,R3+2),INDD(R1,R3+2));
		INCR(R3);
		CMP(R3,R2);
		JUMP_LT(CLONE_LOOP);
		/* done copying, push null str terminator and cleanup */
		MOV(INDD(R0,R3+3),IMM(0));
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_REMAINDER:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); /* get DEST ptr */
		MOV(R1,INDD(R1,1));
		MOV(R2,SCM_ARG(1)); /* get SRC ptr */
		MOV(R2,INDD(R2,1));
		PUSH(REM(R1,R2));
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_STRING_SET:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); // get vtype ptr
		MOV(R2,SCM_ARG(1)); // get int ptr (for index)
		MOV(R2,INDD(R2,1));	// override index ptr with actual value
		MOV(R3,SCM_ARG(2)); // get rep obj ptr
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(R2,INDD(R1,1)); 
		JUMP_GE(FAIL); // fail if index >= strlen
		CMP(R2,0);
		JUMP_LT(FAIL); // fail if index < 0
		MOV(R3,INDD(R3,1));	// override ptr with actual value
		MOV(INDD(R1,R2+2),R3); // vtype_ptr[idx+2]<-obj_ptr_val
		PUSH(IMM(1)); // push true
		JUMP(EXIT);
		FAIL:
		PUSH(IMM(0)); // push false
		EXIT:
		CALL(MAKE_SOB_BOOL);
		DROP(1);		
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_STRING_REF:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); // get vtype ptr
		MOV(R2,SCM_ARG(1)); // get int ptr (for index)
		MOV(R2,INDD(R2,1));	// override index ptr with actual value
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(R2,INDD(R1,1)); 
		JUMP_GE(FAIL); // fail if index >= strlen
		CMP(R2,0);
		JUMP_LT(FAIL); // fail if index < 0
		PUSH(INDD(R1,R2+2)); // stack <- vtype_ptr[idx+2]
		JUMP(EXIT);
		FAIL:
		PUSH(IMM(0)); // push false
		EXIT:
		CALL(MAKE_SOB_CHAR);
		DROP(1);		
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_STRING_LENGTH:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(INDD(SCM_ARG(0),1));
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_CHAR_TO_INTEGER:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move source ptr R1 */
		MOV(R1, INDD(R1,1)); /* get source value in R1 */
		PUSH(IMM(2));
		CALL(MALLOC);
		DROP(1);
		MOV(IND(R0),T_INTEGER);
		MOV(INDD(R0,1),R1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_INTEGER_TO_CHAR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move source ptr R1 */
		MOV(R1, INDD(R1,1)); /* get source value in R1 */
		PUSH(IMM(2));
		CALL(MALLOC);
		DROP(1);
		MOV(IND(R0),T_CHAR);
		MOV(INDD(R0,1),R1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_CONS:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(SCM_ARG(0)); // get car ptr
		PUSH(SCM_ARG(1)); // get cdr ptr
		CALL(MAKE_SOB_PAIR);
		DROP(2)
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_CAR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,INDD(SCM_ARG(0),1));
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_CDR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		MOV(R0,INDD(SCM_ARG(0),2));
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_SET_CAR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		MOV(INDD(SCM_ARG(0),1),SCM_ARG(1));
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_SET_CDR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		MOV(INDD(SCM_ARG(0),2),SCM_ARG(1));
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_MAKE_VECTOR:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1, SCM_ARG(0)); /* move replicate obj count to R1 */
		MOV(R2, SCM_ARG(1)); /* move replicate obj ptr to R2 */
		MOV(R3, IMM(0)); /* init counter */
		BEGIN_LOCAL_LABELS PUSH_LOOP;
		PUSH_LOOP:
		PUSH(R2);
		INCR(R3);
		CMP(R3,R1);
		JUMP_LT(PUSH_LOOP);
		PUSH(R1) /* push char count */
		/* make SOB_VECTOR*/
		CALL(MAKE_SOB_VECTOR);
		DROP(R1+1);
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_VECTOR_SET:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); // get vtype ptr
		MOV(R2,SCM_ARG(1)); // get int ptr (for index)
		MOV(R2,INDD(R2,1));	// override index ptr with actual value
		MOV(R3,SCM_ARG(2)); // get rep obj ptr
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(R2,INDD(R1,1)); 
		JUMP_GE(FAIL); // fail if index >= strlen
		CMP(R2,0);
		JUMP_LT(FAIL); // fail if index < 0
		NOP;
		MOV(INDD(R1,R2+2),R3); // vtype_ptr[idx+2]<-obj_ptr_val
		PUSH(IMM(1)); // push true
		JUMP(EXIT);
		FAIL:
		PUSH(IMM(0)); // push false
		EXIT:
		CALL(MAKE_SOB_BOOL);
		DROP(1);		
		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_VECTOR_REF:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); // get vtype ptr
		MOV(R2,SCM_ARG(1)); // get int ptr (for index)
		MOV(R2,INDD(R2,1));	// override index ptr with actual value
		BEGIN_LOCAL_LABELS FAIL,EXIT;
		CMP(R2,INDD(R1,1)); 
		JUMP_GE(FAIL); // fail if index >= strlen
		CMP(R2,0);
		JUMP_LT(FAIL); // fail if index < 0
		PUSH(INDD(R1,R2+2)); // stack <- vtype_ptr[idx+2]
		JUMP(EXIT);
		FAIL:
		CALL(MAKE_SOB_NIL);
		PUSH(R0);
		EXIT:
		POP(R0);		POPAD;
		POP(FP); /* restore fp and exit */
		RETURN;
		END_LOCAL_LABELS

	PRIM_SCM_PROC_VECTOR_LENGTH:
		/* backup fp and used registers*/
		PUSH(FP);
		MOV(FP,SP);
		PUSH(INDD(SCM_ARG(0),1));
		CALL(MAKE_SOB_INTEGER);
		DROP(1);
		POP(FP); /* restore fp and exit */
		RETURN;

	PRIM_SCM_PROC_APPLY:
		/* start normal, get parameters as scm args */
		PUSH(FP);
		MOV(FP,SP);
		MOV(R1,FPARG(-1)); /* return address */
		MOV(R2,SCM_ARG(0)); /* target func addr */
		MOV(R3,SCM_ARG(1)); /* param lst ptr */
		/* reverse param list */
		PUSH(R3);
		PUSH(1);
		PUSH(SCM_ENV);
		CALL(PRIM_SCM_PROC_REVERSE);
		DROP(3);
		/* now overwrite frame (tc-applic shit) by overriding FP,SP */
		POP(FP);
		MOV(SP,FP);
		/* populate stack from reversed list*/
		BEGIN_LOCAL_LABELS PUSH_LOOP,DONE_POP;
		MOV(R4,0); /* init param counter */
		MOV(R5,R0); /* copy lst head ptr to R5 */
		PUSH_LOOP:
			PUSH(INDD(R5,1));
			MOV(R6,INDD(R5,2));
			INCR(R4); /* incr argc */
			CMP(IND(R6),T_NIL); /* next ptr points to nil? */
			JUMP_EQ(DONE_POP);
			MOV(R5,R6); 
			JUMP(PUSH_LOOP);
		DONE_POP:
		PUSH(R4); /* push argc */
		END_LOCAL_LABELS
		PUSH(SCM_ENV); /* prep frame for jumping */
		PUSH(R1);
		JUMPA(R2); /* jump to target function */

	/* takes a list of arguments and makes nested pairs outta them */
	PRIM_SCM_PROC_LIST:
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARGC); /* get arg count */
		BEGIN_LOCAL_LABELS PAIR_LOOP;
		MOV(R2,R1); /* init counter */
		DECR(R2);
		/* create null for first pair */
		CALL(MAKE_SOB_NIL);
		PAIR_LOOP:
			PUSH(R0);
			PUSH(SCM_ARG(R2));
			CALL(MAKE_SOB_PAIR);
			DROP(2);
			DECR(R2);
			CMP(R2,0);
			JUMP_GE(PAIR_LOOP);
		END_LOCAL_LABELS 
		POPAD;
		POP(FP);
		RETURN;

	/* reverses a given list */
	PRIM_SCM_PROC_REVERSE:
		PUSH(FP);
		MOV(FP,SP);
		PUSHAD;
		MOV(R1,SCM_ARG(0)); /* first link ptr */
		MOV(R2,R1); /* current link ptr */
		BEGIN_LOCAL_LABELS REV_LOOP,EXIT;
		REV_LOOP:
			MOV(R3,INDD(R2,2)); /* next link ptr */
			MOV(INDD(R2,2),R1);
			MOV(R1,R2);
			MOV(R2,R3);
			CMP(IND(R2),T_NIL); /* check if obj type is null */
			JUMP_EQ(EXIT);
			JUMP(REV_LOOP);
		EXIT:
		/* set last ptr to null*/
		MOV(R3,SCM_ARG(0));
		MOV(INDD(R3,2),R2);
		/* return first link ptr as list head ptr */
		MOV(R0,R1);
		END_LOCAL_LABELS
		POPAD;
		POP(FP);
		RETURN;


	CONTINUE:
ADD(IND(0), IMM(1000));

MOV(IND(1), IMM(T_VOID));

MOV(IND(2), IMM(T_NIL));

MOV(IND(3), IMM(T_BOOL));
MOV(IND(4), IMM(0));

MOV(IND(5), IMM(T_BOOL));
MOV(IND(6), IMM(1));

MOV(R0,IND(SOB_NIL)); /* reset R0 before execution */

/* primitive procs table */
MOV(IND(10), IMM(T_CLOSURE));
MOV(IND(11), IMM(666));
MOV(IND(12), LABEL(PRIM_SCM_PROC_EQUIV));
#define prim_equiv 10
MOV(IND(13), IMM(T_CLOSURE));
MOV(IND(14), IMM(666));
MOV(IND(15), LABEL(PRIM_SCM_PROC_GT));
#define prim_gt 13
MOV(IND(16), IMM(T_CLOSURE));
MOV(IND(17), IMM(666));
MOV(IND(18), LABEL(PRIM_SCM_PROC_LT));
#define prim_lt 16
MOV(IND(19), IMM(T_CLOSURE));
MOV(IND(20), IMM(666));
MOV(IND(21), LABEL(PRIM_SCM_PROC_ADD));
#define prim_add 19
MOV(IND(22), IMM(T_CLOSURE));
MOV(IND(23), IMM(666));
MOV(IND(24), LABEL(PRIM_SCM_PROC_SUB));
#define prim_sub 22
MOV(IND(25), IMM(T_CLOSURE));
MOV(IND(26), IMM(666));
MOV(IND(27), LABEL(PRIM_SCM_PROC_MUL));
#define prim_mul 25
MOV(IND(28), IMM(T_CLOSURE));
MOV(IND(29), IMM(666));
MOV(IND(30), LABEL(PRIM_SCM_PROC_DIV));
#define prim_div 28
MOV(IND(31), IMM(T_CLOSURE));
MOV(IND(32), IMM(666));
MOV(IND(33), LABEL(PRIM_SCM_PROC_IS_CHAR));
#define prim_is_char 31
MOV(IND(34), IMM(T_CLOSURE));
MOV(IND(35), IMM(666));
MOV(IND(36), LABEL(PRIM_SCM_PROC_IS_INTEGER));
#define prim_is_integer 34
MOV(IND(37), IMM(T_CLOSURE));
MOV(IND(38), IMM(666));
MOV(IND(39), LABEL(PRIM_SCM_PROC_IS_NULL));
#define prim_is_null 37
MOV(IND(40), IMM(T_CLOSURE));
MOV(IND(41), IMM(666));
MOV(IND(42), LABEL(PRIM_SCM_PROC_IS_PAIR));
#define prim_is_pair 40
MOV(IND(43), IMM(T_CLOSURE));
MOV(IND(44), IMM(666));
MOV(IND(45), LABEL(PRIM_SCM_PROC_IS_PROCEDURE));
#define prim_is_procedure 43
MOV(IND(46), IMM(T_CLOSURE));
MOV(IND(47), IMM(666));
MOV(IND(48), LABEL(PRIM_SCM_PROC_IS_STRING));
#define prim_is_string 46
MOV(IND(49), IMM(T_CLOSURE));
MOV(IND(50), IMM(666));
MOV(IND(51), LABEL(PRIM_SCM_PROC_IS_SYMBOL));
#define prim_is_symbol 49
MOV(IND(52), IMM(T_CLOSURE));
MOV(IND(53), IMM(666));
MOV(IND(54), LABEL(PRIM_SCM_PROC_IS_VECTOR));
#define prim_is_vector 52
MOV(IND(55), IMM(T_CLOSURE));
MOV(IND(56), IMM(666));
MOV(IND(57), LABEL(PRIM_SCM_PROC_IS_ZERO));
#define prim_is_zero 55
MOV(IND(58), IMM(T_CLOSURE));
MOV(IND(59), IMM(666));
MOV(IND(60), LABEL(PRIM_SCM_PROC_IS_NUMBER));
#define prim_is_number 58
MOV(IND(61), IMM(T_CLOSURE));
MOV(IND(62), IMM(666));
MOV(IND(63), LABEL(PRIM_SCM_PROC_IS_EQ));
#define prim_is_eq 61
MOV(IND(64), IMM(T_CLOSURE));
MOV(IND(65), IMM(666));
MOV(IND(66), LABEL(PRIM_SCM_PROC_MAKE_STRING));
#define prim_make_string 64
MOV(IND(67), IMM(T_CLOSURE));
MOV(IND(68), IMM(666));
MOV(IND(69), LABEL(PRIM_SCM_PROC_STRING_TO_SYMBOL));
#define prim_string_to_symbol 67
MOV(IND(70), IMM(T_CLOSURE));
MOV(IND(71), IMM(666));
MOV(IND(72), LABEL(PRIM_SCM_PROC_SYMBOL_TO_STRING));
#define prim_symbol_to_string 70
MOV(IND(73), IMM(T_CLOSURE));
MOV(IND(74), IMM(666));
MOV(IND(75), LABEL(PRIM_SCM_PROC_REMAINDER));
#define prim_remainder 73
MOV(IND(76), IMM(T_CLOSURE));
MOV(IND(77), IMM(666));
MOV(IND(78), LABEL(PRIM_SCM_PROC_STRING_SET));
#define prim_string_set 76
MOV(IND(79), IMM(T_CLOSURE));
MOV(IND(80), IMM(666));
MOV(IND(81), LABEL(PRIM_SCM_PROC_STRING_REF));
#define prim_string_ref 79
MOV(IND(82), IMM(T_CLOSURE));
MOV(IND(83), IMM(666));
MOV(IND(84), LABEL(PRIM_SCM_PROC_STRING_LENGTH));
#define prim_string_length 82
MOV(IND(85), IMM(T_CLOSURE));
MOV(IND(86), IMM(666));
MOV(IND(87), LABEL(PRIM_SCM_PROC_CHAR_TO_INTEGER));
#define prim_char_to_integer 85
MOV(IND(88), IMM(T_CLOSURE));
MOV(IND(89), IMM(666));
MOV(IND(90), LABEL(PRIM_SCM_PROC_INTEGER_TO_CHAR));
#define prim_integer_to_char 88
MOV(IND(91), IMM(T_CLOSURE));
MOV(IND(92), IMM(666));
MOV(IND(93), LABEL(PRIM_SCM_PROC_CONS));
#define prim_cons 91
MOV(IND(94), IMM(T_CLOSURE));
MOV(IND(95), IMM(666));
MOV(IND(96), LABEL(PRIM_SCM_PROC_CAR));
#define prim_car 94
MOV(IND(97), IMM(T_CLOSURE));
MOV(IND(98), IMM(666));
MOV(IND(99), LABEL(PRIM_SCM_PROC_CDR));
#define prim_cdr 97
MOV(IND(100), IMM(T_CLOSURE));
MOV(IND(101), IMM(666));
MOV(IND(102), LABEL(PRIM_SCM_PROC_SET_CAR));
#define prim_set_car 100
MOV(IND(103), IMM(T_CLOSURE));
MOV(IND(104), IMM(666));
MOV(IND(105), LABEL(PRIM_SCM_PROC_SET_CDR));
#define prim_set_cdr 103
MOV(IND(106), IMM(T_CLOSURE));
MOV(IND(107), IMM(666));
MOV(IND(108), LABEL(PRIM_SCM_PROC_MAKE_VECTOR));
#define prim_make_vector 106
MOV(IND(109), IMM(T_CLOSURE));
MOV(IND(110), IMM(666));
MOV(IND(111), LABEL(PRIM_SCM_PROC_VECTOR_SET));
#define prim_vector_set 109
MOV(IND(112), IMM(T_CLOSURE));
MOV(IND(113), IMM(666));
MOV(IND(114), LABEL(PRIM_SCM_PROC_VECTOR_REF));
#define prim_vector_ref 112
MOV(IND(115), IMM(T_CLOSURE));
MOV(IND(116), IMM(666));
MOV(IND(117), LABEL(PRIM_SCM_PROC_VECTOR_LENGTH));
#define prim_vector_length 115
MOV(IND(118), IMM(T_CLOSURE));
MOV(IND(119), IMM(666));
MOV(IND(120), LABEL(PRIM_SCM_PROC_APPLY));
#define prim_apply 118
MOV(IND(121), IMM(T_CLOSURE));
MOV(IND(122), IMM(666));
MOV(IND(123), LABEL(PRIM_SCM_PROC_LIST));
#define prim_list 121
MOV(IND(124), IMM(T_CLOSURE));
MOV(IND(125), IMM(666));
MOV(IND(126), LABEL(PRIM_SCM_PROC_REVERSE));
#define prim_reverse 124

/* ################## COMPILER GENERATED CODE STARTS HERE ################## */

PUSH(IMM(18));
CALL(MAKE_SOB_INTEGER);
DROP(1);
PUSH(R0);

PUSH(IMM(17));
CALL(MAKE_SOB_INTEGER);
DROP(1);
PUSH(R0);

PUSH(IMM(11));
CALL(MAKE_SOB_INTEGER);
DROP(1);
PUSH(R0);

PUSH(IMM(5));
CALL(MAKE_SOB_INTEGER);
DROP(1);
PUSH(R0);


PUSH(4);
PUSH(666);
CALL(PRIM_SCM_PROC_LIST);
MOV(R5,R0);
PUSH(R0);
CALL(WRITE_SOB);
CALL(NEWLINE);
DROP(5);
MOV(R0,R5);
PUSH(R5);
PUSH(LABEL(PRIM_SCM_PROC_LT));
PUSH(0);
PUSH(666);
CALL(PRIM_SCM_PROC_APPLY);



/* ################## GENERATED CODE END HERE ################## */

	CMP(R0,IND(SOB_NIL));
	JUMP_EQ(STOP_AND_EXIT);
	PUSH(R0);
	CALL(WRITE_SOB);
	CALL(NEWLINE);
	STOP_AND_EXIT:
	STOP_MACHINE;

	return 0;
}