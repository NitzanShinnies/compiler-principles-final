
(define label-generator-label-types-list '(
	if3
	or 
	begin
	applic
	tc-applic
	lambda-simple
	lambda-opt
	lambda-variadic
))

(define label-generator-label-prefix-list '(
		(if3 "L_IF3_DIF_" "L_IF3_EXIT_")
		(or "L_OR_END_" "OR_")
		(begin "L_BEGIN_")
		(applic "L_APPLIC_OK_")
		(tc-applic "L_TC_APPLIC_OK_" "L_TC_APPLIC_LOOP_")
		(lambda-simple "L_LAMBDA_SIMPLE_BODY_" "L_LAMBDA_SIMPLE_EXIT_")
		(lambda-opt "L_LAMBDA_OPT_BODY_" "L_LAMBDA_OPT_EXIT_")
		(lambda-variadic "L_LAMBDA_VAR_BODY_" "L_LAMBDA_VAR_EXIT_")
		))

(define label-generator-new-line (list->string (list #\newline)))

(define label-generator-add-num-to-label ; After recieving a number, this function adds it to 'name', I made it like this so it will work with 'map'
	(lambda (num)
		(lambda (name)
			(string-append name (number->string num))
	)))

(define label-generator-get-label-type-count ; returns the counter of a certain label-type.
	(lambda (_label-type)
		(cdr (assoc _label-type label-count))
	))

(define label-generator-generate-labels ; returns a generated list of labels due to the recieved label-type
	(lambda (_label-type)
		(let ((idx (cdr (assoc _label-type label-generator-label-prefix-list))))
			(map (label-generator-add-num-to-label (label-generator-get-label-type-count _label-type)) idx)
	)))

(define label-count (map (lambda (_label) (cons _label 0)) label-generator-label-types-list))

(define reset-label-count
	(lambda ()
		(set! label-count (map (lambda (_label) (cons _label 0)) label-generator-label-types-list))))

(define make-labels 
	(lambda (_label-type)
		(let ((idx (assoc _label-type label-count)))
			(set-cdr! idx (+ (cdr idx) 1)))
		(label-generator-generate-labels _label-type)
		))