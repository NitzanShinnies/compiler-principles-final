(define private-fvar-lst `())
(define extended-fvar-lst `()) 

(define get-fvar-addr
	(lambda (_fvar )
		(caadr (assoc _fvar extended-fvar-lst))))

; creates fvar symbols and "allocates" memory to them
(define make-fvar-init-code
	(lambda (_extended-fvar-lst)
		(apply string-append 
			(map 
				(lambda (_fvar) 
					(let (
						(_addr (caadr _fvar))
						(_val (cdadr _fvar)))						
						(string-append 
							(format "\t\tMOV(IND(~s),IMM(" _addr)
							(if (symbol? _val) (formatter (symbol->string _val)) (formatter _val))
							"));\n"
						)))
				 (reverse _extended-fvar-lst)))))

; searches prim-procs-list for fvar, if not found returns a new address for the ptr
(define make-fvar-entry 
	(lambda (_fvar)
		(let* (
				(_fvar-lookup-val (assoc _fvar prim-scm-procs-keyvalue-master-list))
				(_fvar-init-val (if (eq? _fvar-lookup-val #f) "T_NIL" (string->symbol (cadr _fvar-lookup-val)))))
			 (begin 
				(set! last-allocated-addr (+ last-allocated-addr 1))
			 	(cons last-allocated-addr _fvar-init-val) ; allocate addr
			))))	 


(define remove-empty-entries
	(lambda (_lst)
		(cond
			((null? _lst) `())
			((null? (car _lst)) (remove-empty-entries (cdr _lst)))
			(else (cons (car _lst) (remove-empty-entries (cdr _lst))))
			))) 


(define make-fvar-prim-procs-list ; maps compact prim procs list
	(lambda (_proc-lst _fvar-lst)
		(if (null? _proc-lst) `()
			(let* ((_proc-entry (car _proc-lst))
					(_proc-symbol (car (cddddr _proc-entry)))
					(_proc-fvar (assoc _proc-symbol _fvar-lst))
					(_proc-deps (assoc _proc-symbol prim-scm-procs-dep-list))
					(_proc-dep-list (if (eq? #f _proc-deps) `()
										(remove-empty-entries (append (map (lambda (_dep) (find-prim-scm-proc _dep prim-scm-proc-master-list)) (cadr _proc-deps))))))
					)
				(if (eq? _proc-fvar #f) 
					(make-fvar-prim-procs-list (cdr _proc-lst) _fvar-lst) 
						(let ((_p-entry (if (eq? #f _proc-deps) `(,_proc-entry) (cons _proc-entry _proc-dep-list))))
							(append _p-entry (make-fvar-prim-procs-list (cdr _proc-lst) _fvar-lst )))
					)))))					 


(define make-fvar-extended-list ; makes _addr _name
	(lambda (_fvar-list)
		(append `() (map (lambda (_fvar) `(,@_fvar ,(make-fvar-entry (car _fvar)))) _fvar-list))))

(define get-fvar-list
	(lambda (_expr)
		(begin
			(set! private-fvar-lst `())
			(fvar-traverse-expr-list _expr)
			private-fvar-lst)))

(define fvar-traverse-expr-list
	(lambda (_expr)
		(cond

			((is? _expr 'fvar) (begin (set! private-fvar-lst (append private-fvar-lst (list (cdr _expr)))) `())) ; return empty value to inhibit failures as this function returs basically dick.

			((or (is? _expr 'const) (is? _expr 'bvar) (is? _expr 'pvar)) `())

			((is? _expr 'lambda-opt) `(,@(fvar-traverse-expr-list (cadddr _expr))))

			((is? _expr 'if) 
				`(
					,@(fvar-traverse-expr-list (cadr _expr))
					,@(fvar-traverse-expr-list (caddr _expr))
					,@(fvar-traverse-expr-list (cadddr _expr))
					))

			((or (is? _expr 'lambda-simple) (is? _expr 'lambda-variadic))
				`(,@(fvar-traverse-expr-list (caddr _expr))))

			((or (is? _expr 'or) (is? _expr 'seq)) `(,@(map fvar-traverse-expr-list (cadr _expr))))

			((is? _expr 'define) (let ((p1 (cadr _expr)) (p2 (caddr _expr))) `(,@(fvar-traverse-expr-list p1) ,@(fvar-traverse-expr-list p2))))
			(else 
				(if (is-last-element? _expr) ; if cdr is null, check if expr is null. if it isn't, it's a nested list so check car
					(if (null? _expr) _expr (fvar-traverse-expr-list (car _expr)))
				`(,(fvar-traverse-expr-list (cadr _expr)) , ; cdr isn't null, so assume it's applic or matching form
				(map (lambda (_exp) (fvar-traverse-expr-list _exp)) (caddr _expr)))
				)
			)
		)))	
