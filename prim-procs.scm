; --------------------------------------- primitive functions master list  -------------------------------------- 

(define prim-scm-proc-master-list `(
	 (0 prim-scm-proc-equiv "prim_equiv" "PRIM_SCM_PROC_EQUIV" =)  												; = (variadic)
	 (0 prim-scm-proc-gt "prim_gt" "PRIM_SCM_PROC_GT" >) 														; > (variadic)
	 (0 prim-scm-proc-lt "prim_lt" "PRIM_SCM_PROC_LT" <) 														; < (variadic)
	 (0 prim-scm-proc-add "prim_add" "PRIM_SCM_PROC_ADD" +) 													; + (variadic)
	 (0 prim-scm-proc-sub "prim_sub" "PRIM_SCM_PROC_SUB" -)														; - (variadic)
	 (0 prim-scm-proc-mul "prim_mul" "PRIM_SCM_PROC_MUL" *) 													; * (variadic)
	 (0 prim-scm-proc-div "prim_div" "PRIM_SCM_PROC_DIV" /) 													; / (variadic)
	 (0 prim-scm-proc-is-char "prim_is_char" "PRIM_SCM_PROC_IS_CHAR" char?) 									; char?
	 (0 prim-scm-proc-is-boolean "prim_is_boolean" "PRIM_SCM_PROC_IS_BOOLEAN" boolean?) 						; boolean?
	 (0 prim-scm-proc-is-integer "prim_is_integer" "PRIM_SCM_PROC_IS_INTEGER" integer?) 						; integer?
	 (0 prim-scm-proc-is-null "prim_is_null" "PRIM_SCM_PROC_IS_NULL" null?)										; null?
	 (0 prim-scm-proc-is-pair "prim_is_pair" "PRIM_SCM_PROC_IS_PAIR" pair?) 									; pair?
	 (0 prim-scm-proc-is-procedure "prim_is_procedure" "PRIM_SCM_PROC_IS_PROCEDURE" procedure?) 				; procedure?
	 (0 prim-scm-proc-is-string "prim_is_string" "PRIM_SCM_PROC_IS_STRING" string?) 							; string?
	 (0 prim-scm-proc-is-symbol "prim_is_symbol" "PRIM_SCM_PROC_IS_SYMBOL" symbol?) 							; symbol?
	 (0 prim-scm-proc-is-vector "prim_is_vector""PRIM_SCM_PROC_IS_VECTOR" vector?)  							; vector?
	 (0 prim-scm-proc-is-zero "prim_is_zero" "PRIM_SCM_PROC_IS_ZERO" zero?) 									; zero?
	 (0 prim-scm-proc-is-number "prim_is_number" "PRIM_SCM_PROC_IS_NUMBER" number?) 							; number?
	 (0 prim-scm-proc-eq "prim_is_eq" "PRIM_SCM_PROC_IS_EQ" eq?) 												; eq?
	 (0 prim-scm-proc-make-string "prim_make_string" "PRIM_SCM_PROC_MAKE_STRING" make-string) 					; make-string
	 (0 prim-scm-proc-string_to_symbol "prim_string_to_symbol" "PRIM_SCM_PROC_STRING_TO_SYMBOL" string->symbol)	; string->symbol
	 (0 prim-scm-proc-symbol_to_string "prim_symbol_to_string" "PRIM_SCM_PROC_SYMBOL_TO_STRING" symbol->string)	; symbol->string
	 (0 prim-scm-proc-remainder "prim_remainder" "PRIM_SCM_PROC_REMAINDER" remainder) 							; remainder
	 (0 prim-scm-proc-string-set "prim_string_set" "PRIM_SCM_PROC_STRING_SET" string-set!) 						; string-set!
	 (0 prim-scm-proc-string-ref "prim_string_ref" "PRIM_SCM_PROC_STRING_REF" string-ref) 						; string-ref
	 (0 prim-scm-proc-string-equal "prim_string_equal" "PRIM_SCM_PROC_STRING_EQUAL" string=?)					; string=?
	 (0 prim-scm-proc-string-length "prim_string_length" "PRIM_SCM_PROC_STRING_LENGTH" string-length) 			; string-length
	 (0 prim-scm-proc-char_to_integer "prim_char_to_integer" "PRIM_SCM_PROC_CHAR_TO_INTEGER" char->integer) 	; char->integer
	 (0 prim-scm-proc-integer_to_char "prim_integer_to_char" "PRIM_SCM_PROC_INTEGER_TO_CHAR" integer->char) 	; integer->char
	 (0 prim-scm-proc-cons "prim_cons" "PRIM_SCM_PROC_CONS" cons) 												; cons
	 (0 prim-scm-proc-car "prim_car" "PRIM_SCM_PROC_CAR" car) 													; car
	 (0 prim-scm-proc-cdr "prim_cdr" "PRIM_SCM_PROC_CDR" cdr) 													; cdr
	 (0 prim-scm-proc-set-car "prim_set_car" "PRIM_SCM_PROC_SET_CAR" set-car!) 									; set-car!
	 (0 prim-scm-proc-set-cdr "prim_set_cdr" "PRIM_SCM_PROC_SET_CDR" set-cdr!) 									; set-cdr!
	 (0 prim-scm-proc-make-vector "prim_make_vector" "PRIM_SCM_PROC_MAKE_VECTOR" make-vector) 					; make-vector
	 (0 prim-scm-proc-vector-set "prim_vector_set" "PRIM_SCM_PROC_VECTOR_SET" vector-set!) 						; vector-set!
	 (0 prim-scm-proc-vector-ref "prim_vector_ref" "PRIM_SCM_PROC_VECTOR_REF" vector-ref) 						; vector-ref
	 (0 prim-scm-proc-vector-length "prim_vector_length" "PRIM_SCM_PROC_VECTOR_LENGTH" vector-length) 			; vector-length
	 (0 prim-scm-proc-apply "prim_apply" "PRIM_SCM_PROC_APPLY" apply)											; apply
	 (0 prim-scm-proc-lst "prim_list" "PRIM_SCM_PROC_LIST" list)												; list
	 (0 prim-scm-proc-reverse "prim_reverse" "PRIM_SCM_PROC_REVERSE" reverse)									; reverse
	 (0 prim-scm-proc-not "prim_not" "PRIM_SCM_PROC_NOT" not)													; not
	 (0 prim-scm-proc-char-equal "prim_char_equal" "PRIM_SCM_PROC_CHAR_EQUAL" char=?)							; char=?
	 (0 prim-scm-proc-vector-to-list "prim_vector_to_list" "PRIM_SCM_PROC_VECTOR_TO_LIST" vector->list)			; vector->list
	))

(define prim-scm-procs-keyvalue-master-list
	(append `() (map (lambda (_proc) `(,@(cddddr _proc) ,(caddr _proc))) prim-scm-proc-master-list)))

(define find-prim-scm-proc
	(lambda (_proc-name _proc-list)
		(if (null? _proc-list) `()
			(let* (
				(_proc-entry (car _proc-list))
				(_proc-name-from-list (car (cddddr _proc-entry)))
				)
				(if (eq?  _proc-name _proc-name-from-list) _proc-entry (find-prim-scm-proc _proc-name (cdr _proc-list))) 	
			))))


; dependencies list for primitive procs!
; apply, for example, requires LIST_REVERSE , while number? required integer?
(define prim-scm-procs-dep-list 
	`(
		(apply ,`(reverse))
		(number? ,`(integer?))
		(string->symbol ,`(string=?))
	))

; ---------------------------- primitive function factories -------------------------------

; creates all relation functions 
(define make-prim-scm-proc-rel
	(lambda (_func-name _jmp-type)
		(string-append
				"\t" _func-name ":\n"
				"\t\t/* backup fp and used registers*/\n"
				"\t\tPUSH(FP);\n"
				"\t\tMOV(FP,SP);\n"
				"\t\tPUSHAD;\n"
				"\t\tBEGIN_LOCAL_LABELS NOT_SINGLE_ARG, CMP_LOOP, EQ_FAIL, EQ_EXIT;\n"
				"\t\tMOV(R1,SCM_ARGC); /* move ARGC to R1 */\n"
				"\t\tCMP(R1,IMM(1)); \n"
				"\t\tJUMP_GT(NOT_SINGLE_ARG);\n"
				"\t\tMOV(R0,IMM(SOB_TRUE));\n"
				"\t\tJUMP(EQ_EXIT);\n"
				"\t\tNOT_SINGLE_ARG:\n"
				"\t\tMOV(R2,1); /* init counter */\n"
				"\t\tMOV(R4,SCM_ARG(0)); /* move first value PTR to test-again register */\n"
				"\t\tMOV(R4,INDD(R4,1)); /* get actual value */\n";
				"\t\tCMP_LOOP:\n"
				"\t\t\tMOV(R3,R4); /* move old test value to test register */\n"
				"\t\t\tMOV(R4,SCM_ARG(R2)); /* get new comparison value PTR */\n"
				"\t\t\tMOV(R4,INDD(R4,1));\n";
				"\t\t\tCMP(R3,R4);\n"
				"\t\t\t" _jmp-type "(EQ_FAIL); /* isn't in relation, fail */\n"
				"\t\t\tINCR(R2);\n"
				"\t\t\tCMP(R2,R1);\n"
				"\t\tJUMP_LT(CMP_LOOP); /* more args, loop */\n"
				"\t\tMOV(R0,IMM(SOB_TRUE)); /* in relation, set result to true and goto exit */\n"
				"\t\tJUMP(EQ_EXIT);\n"
				"\t\tEQ_FAIL:\n"
				"\t\tMOV(R0,IMM(SOB_FALSE));\n"
				"\t\tEQ_EXIT:\n"
				"\t\tPOPAD;\n"
				"\t\tPOP(FP); /* restore fp and exit */\n"
				"\t\tRETURN;\n"
				"\t\tEND_LOCAL_LABELS\n"
		)))

; creates all arithmetic functions
(define make-prim-scm-proc-arithmetic
	(lambda (_func-name _op-type _id-element)
		(string-append
		"\t" _func-name ":\n"
		"\t\t/* backup fp and used registers */\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"	
		"\t\tBEGIN_LOCAL_LABELS CHECK_SINGLE_ARG, CALC_LOOP, MAKE_RESULT;\n"
		"\t\tMOV(R1,SCM_ARGC); /* move ARGC to R1 */\n"
		"\t\tCMP(R1,IMM(0));\n"
		"\t\tJUMP_NE(CHECK_SINGLE_ARG);\n"
		"\t\tMOV(R3,IMM(" (number->string _id-element) "));\n"
		"\t\tJUMP(MAKE_RESULT);\n"
		"\t\tCHECK_SINGLE_ARG:\n"
		"\t\tMOV(R3,SCM_ARG(0)); /* init result register */\n"
		"\t\tMOV(R3,INDD(R3,1));\n"
		"\t\tMOV(R2,IMM(1)); /* init counter */\n"
		"\t\tCMP(R1,IMM(1));\n"
		"\t\tJUMP_NE(CALC_LOOP);\n"
		"\t\t/* single arg op, using the id-element */\n"
		"\t\tMOV(R2,IMM(" (number->string _id-element) "));\n"
		"\t\t" _op-type "(R2,R3);\n"
		"\t\tMOV(R3,R2);\n"
		"\t\tJUMP(MAKE_RESULT);\n"
		"\t\tCALC_LOOP:\n"
		"\t\t\tMOV(R4,SCM_ARG(R2)); /* move arg PTR to register */\n"
		"\t\t\tMOV(R4,INDD(R4,1)); /* get actual value */\n"
		"\t\t\t" _op-type "(R3,R4);\n"
		"\t\t\tINCR(R2);\n"
		"\t\t\tCMP(R2,R1);\n"
		"\t\tJUMP_LT(CALC_LOOP); /* more args, loop */\n"
		"\t\tMAKE_RESULT:\n"
		"\t\t/* make SOB_INTEGER from result */\n"
		"\t\tPUSH(R3);\n"
		"\t\tCALL(MAKE_SOB_INTEGER);\n"
		"\t\tDROP(1);\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP); /* restore fp and exit */\n"
		"\t\tRETURN;\n"
		"\t\tEND_LOCAL_LABELS\n"
		)))

; creates all type? functions
(define make-prim-scm-proc-is
	(lambda (_func-name _scheme-type)
		(string-append
			"\tPRIM_SCM_PROC_IS_" _func-name ":\n"
			"\t\tPUSH(FP);\n"
			"\t\tMOV(FP,SP);\n"
			"\t\tMOV(R0,SCM_ARG(0));\n"
			"\t\tBEGIN_LOCAL_LABELS FAIL,EXIT;\n"
			"\t\tCMP(IND(R0)," _scheme-type ");\n"
			"\t\tJUMP_NE(FAIL);\n"
			"\t\tMOV(R0,IMM(SOB_TRUE));\n"
			"\t\tJUMP(EXIT);\n"
			"\t\tFAIL:\n"
			"\t\tMOV(R0,IMM(SOB_FALSE));\n"
			"\t\tEXIT:\n"
			"\t\tPOP(FP);\n"
			"\t\tRETURN;\n"
			"\t\tEND_LOCAL_LABELS\n"
		)))

; creates char->integer and integer->char
(define make-prim-scm-proc-convert-char-int
	(lambda (_source-type _target-type)
		(string-append
			"\tPRIM_SCM_PROC_" _source-type "_TO_" _target-type ":\n"
			"\t\t/* backup fp and used registers*/\n"
			"\t\tPUSH(FP);\n"
			"\t\tMOV(FP,SP);\n"
			"\t\tPUSHAD;\n"
			"\t\tMOV(R1, SCM_ARG(0)); /* move source ptr R1 */\n"
			"\t\tMOV(R1, INDD(R1,1)); /* get source value in R1 */\n"
			"\t\tPUSH(IMM(2));\n"
			"\t\tCALL(MALLOC);\n"
			"\t\tDROP(1);\n"
			"\t\tMOV(IND(R0),T_" _target-type ");\n"
			"\t\tMOV(INDD(R0,1),R1);\n"
			"\t\tPOPAD;\n"
			"\t\tPOP(FP); /* restore fp and exit */\n"
			"\t\tRETURN;\n"
		)))

; creates car, cdr
(define make-prim-scm-proc-pair-set
	(lambda (_getter _index)
		(string-append
			"\tPRIM_SCM_PROC_SET_" _getter ":\n"
			"\t\t/* backup fp and used registers*/\n"
			"\t\tPUSH(FP);\n"
			"\t\tMOV(FP,SP);\n"
			"\t\tMOV(INDD(SCM_ARG(0)," _index "),SCM_ARG(1));\n"
			"\t\tPOP(FP); /* restore fp and exit */\n"
			"\t\tRETURN;\n"
		)))

; creates set-car!, set-cdr!
(define make-prim-scm-proc-pair-get
	(lambda (_getter _index)
		(string-append
			"\tPRIM_SCM_PROC_" _getter ":\n"
			"\t\t/* backup fp and used registers*/\n"
			"\t\tPUSH(FP);\n"
			"\t\tMOV(FP,SP);\n"
			"\t\tMOV(R0,INDD(SCM_ARG(0)," _index "));\n"
			"\t\tPOP(FP); /* restore fp and exit */\n"
			"\t\tRETURN;\n"
		)))

; creates make-string, make-vector
(define make-prim-scm-proc-vtypes
		(lambda (_type-label _override-val)
			(string-append
				"\tPRIM_SCM_PROC_MAKE_" _type-label ":\n"
				"\t\t/* backup fp and used registers*/\n"
				"\t\tPUSH(FP);\n"
				"\t\tMOV(FP,SP);\n"
				"\t\tPUSHAD;\n"
				"\t\tBEGIN_LOCAL_LABELS IS_NORMAL, SET_CTR, PUSH_LOOP;\n"
				"\t\tMOV(R1, SCM_ARG(0)); /* move replicate obj count to R1 */\n"
				"\t\tMOV(R1,INDD(R1,1)); /* get value */\n"
				"\t\tCMP(SCM_ARGC,IMM(1));\n"
				"\t\tJUMP_GT(IS_NORMAL);\n"
				(if _override-val
					"MOV(R2,IMM(0));"
					(string-append
						"\t\tPUSH(IMM(0));\n"
						"\t\tCALL(MAKE_SOB_INTEGER);\n"
						"\t\tDROP(1);\n"
						"\t\tMOV(R2,R0);\n"
					))
				"\t\tJUMP(SET_CTR);\n"
				"\t\tIS_NORMAL:\n"
				"\t\tMOV(R2, SCM_ARG(1)); /* move replicate obj ptr to R2 */\n"
				(if _override-val "\t\tMOV(R2,INDD(R2,1));\n" "")
				"\t\tSET_CTR:\n"
				"\t\tMOV(R3, IMM(0)); /* init counter */\n"
				"\t\tPUSH_LOOP:\n"
				"\t\tPUSH(R2);\n"
				; if immutable - insert replication code here! 
				"\t\tINCR(R3);\n"
				"\t\tCMP(R3,R1);\n"
				"\t\tJUMP_LT(PUSH_LOOP);\n"
				"\t\tPUSH(R1) /* push char count */\n"
				"\t\t/* make SOB_" _type-label "*/\n"
				"\t\tCALL(MAKE_SOB_" _type-label ");\n"
				"\t\tDROP(R1+1);\n"
				"\t\tPOPAD;\n"
				"\t\tPOP(FP); /* restore fp and exit */\n"
				"\t\tRETURN;\n"
			"\t\tEND_LOCAL_LABELS\n"
		)))

; creates string-set!, vector-set!
(define make-prim-scm-proc-vtypes-set
		(lambda (_type-label _obj-ptr-val)
			(string-append
				"\tPRIM_SCM_PROC_" _type-label "_SET:\n"
				"\t\t/* backup fp and used registers*/\n"
				"\t\tPUSH(FP);\n"
				"\t\tMOV(FP,SP);\n"
				"\t\tPUSHAD;\n"
				"\t\tMOV(R1,SCM_ARG(0)); // get vtype ptr\n"
				"\t\tMOV(R2,SCM_ARG(1)); // get int ptr (for index)\n"
				"\t\tMOV(R2,INDD(R2,1));\t// override index ptr with actual value\n"
				"\t\tMOV(R3,SCM_ARG(2)); // get rep obj ptr\n"
				"\t\tBEGIN_LOCAL_LABELS EXIT;\n"
				"\t\tCMP(R2,INDD(R1,1)); \n"
				"\t\tJUMP_GE(EXIT); // fail if index >= strlen\n"
				"\t\tCMP(R2,0);\n"
				"\t\tJUMP_LT(EXIT); // fail if index < 0\n"
				_obj-ptr-val
				"\t\tMOV(INDD(R1,R2+2),R3); // vtype_ptr[idx+2]<-obj_ptr_val\n"
				"\t\tEXIT:\n"
				"\t\tMOV(R0,IMM(SOB_VOID));\n"
				"\t\tPOPAD;\n"
				"\t\tPOP(FP); /* restore fp and exit */\n"
				"\t\tRETURN;\n"
				"\t\tEND_LOCAL_LABELS\n"
			)))

; creates string-ref, vector-ref
(define make-prim-scm-proc-vtypes-ref
		(lambda (_type-label _obj-ptr-val _obj_fail)
			(string-append
				"\tPRIM_SCM_PROC_" _type-label "_REF:\n"
				"\t\t/* backup fp and used registers*/\n"
				"\t\tPUSH(FP);\n"
				"\t\tMOV(FP,SP);\n"
				"\t\tPUSHAD;\n"
				"\t\tMOV(R1,SCM_ARG(0)); // get vtype ptr\n"
				"\t\tMOV(R2,SCM_ARG(1)); // get int ptr (for index)\n"
				"\t\tMOV(R2,INDD(R2,1));\t// override index ptr with actual value\n"
				"\t\tBEGIN_LOCAL_LABELS FAIL,EXIT;\n"
				"\t\tCMP(R2,INDD(R1,1)); \n"
				"\t\tJUMP_GE(FAIL); // fail if index >= strlen\n"
				"\t\tCMP(R2,0);\n"
				"\t\tJUMP_LT(FAIL); // fail if index < 0\n"
				"\t\tPUSH(INDD(R1,R2+2)); // stack <- vtype_ptr[idx+2]\n"
				"\t\tJUMP(EXIT);\n"
				"\t\tFAIL:\n"
				_obj_fail			
				"\t\tEXIT:\n"
				_obj-ptr-val
				"\t\tPOPAD;\n"
				"\t\tPOP(FP); /* restore fp and exit */\n"
				"\t\tRETURN;\n"
				"\t\tEND_LOCAL_LABELS\n"
			)))

; creates string-length, vector-length
(define make-prim-scm-proc-vtypes-length
		(lambda (_type-label)
			(string-append
			"\tPRIM_SCM_PROC_" _type-label "_LENGTH:\n"
			"\t\t/* backup fp and used registers*/\n"
			"\t\tPUSH(FP);\n"
			"\t\tMOV(FP,SP);\n"
			"\t\tPUSH(INDD(SCM_ARG(0),1));\n"
			"\t\tCALL(MAKE_SOB_INTEGER);\n"
			"\t\tDROP(1);\n"
			"\t\tPOP(FP); /* restore fp and exit */\n"
			"\t\tRETURN;\n"
		)))

; ------------------------------ implicit primitive function definitions -----------------------------------------

; ***** arithmetic functions *****
(define prim-scm-proc-is-zero
	(string-append
		"\tPRIM_SCM_PROC_IS_ZERO:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1,SCM_ARG(0));\n"
		"\t\tBEGIN_LOCAL_LABELS FAIL,EXIT;\n"
		"\t\tCMP(IND(R1),T_INTEGER);\n"
		"\t\tJUMP_NE(FAIL);\n"
		"\t\tCMP(INDD(R1,1),IMM(0));\n"
		"\t\tJUMP_NE(FAIL);\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tFAIL:\n"
		"\t\tMOV(R0,IMM(SOB_FALSE));\n"
		"\t\tEXIT:\n"
		"\t\tPOPAD;\n";
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
		"\t\tEND_LOCAL_LABELS\n"
	))

(define prim-scm-proc-is-number
	(string-append
		"\tPRIM_SCM_PROC_IS_NUMBER:\n"
		"\t\tJUMP(PRIM_SCM_PROC_IS_INTEGER);\n"
	))

(define prim-scm-proc-eq
	(string-append
		"\tPRIM_SCM_PROC_IS_EQ:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\t/* get args from stack */\n"
		"\t\tMOV(R1,SCM_ARG(0));\n"
		"\t\tMOV(R2,SCM_ARG(1));\n"
		"\t\tBEGIN_LOCAL_LABELS FAIL,EXIT;\n"
		"\t\tCMP(IND(R1),IND(R2));\n"
		"\t\tJUMP_NE(FAIL);\n"
		"\t\tCMP(INDD(R1,1),INDD(R2,1));\n"
		"\t\tJUMP_NE(FAIL);\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tFAIL:\n"
		"\t\tMOV(R0,IMM(SOB_FALSE));\n"
		"\t\tEXIT:\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
		"\t\tEND_LOCAL_LABELS\n"
	))

(define prim-scm-proc-string-equal
	(string-append
		"\tPRIM_SCM_PROC_STRING_EQUAL:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1,SCM_ARG(0));\n"
		"\t\tMOV(R2,SCM_ARG(1));\n"
		"\t\tBEGIN_LOCAL_LABELS NOT_EQUAL, CMP_LOOP;\n"
		"\t\tCMP(IND(R1),IND(R2)); /* compare types */\n"
		"\t\tJUMP_NE(NOT_EQUAL);\n"
		"\t\tCMP(INDD(R1,1),INDD(R2,1)); /* compare lengths */\n"
		"\t\tJUMP_NE(NOT_EQUAL);\n"
		"\t\tMOV(R3,2); /* offset 2 is first char */\n"
		"\t\tCMP_LOOP:\n"
		"\t\t\tCMP(INDD(R1,R3),INDD(R2,R3));\n"
		"\t\t\tJUMP_NE(NOT_EQUAL);\n"
		"\t\t\tINCR(R3);\n"
		"\t\t\tCMP(R3,INDD(R1,1)+2);\n"
		"\t\t\tJUMP_LT(CMP_LOOP);\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tNOT_EQUAL:\n"
		"\t\tMOV(R0,IMM(SOB_FALSE));\n"
		"\t\tEXIT:\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP);\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tRETURN;\n"
	))
(define prim-scm-proc-remainder	
	(string-append
	"\tPRIM_SCM_PROC_REMAINDER:\n"
		"\t\t/* backup fp and used registers*/\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1,SCM_ARG(0)); /* get DEST ptr */\n"
		"\t\tMOV(R1,INDD(R1,1));\n"
		"\t\tMOV(R2,SCM_ARG(1)); /* get SRC ptr */\n"
		"\t\tMOV(R2,INDD(R2,1));\n"
		"\t\tPUSH(REM(R1,R2));\n"
		"\t\tCALL(MAKE_SOB_INTEGER);\n"
		"\t\tDROP(1);\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP); /* restore fp and exit */\n"
		"\t\tRETURN;\n"
	))

; ***** pair functions *****
(define prim-scm-proc-cons 
	(string-append
		"\tPRIM_SCM_PROC_CONS:\n"
		"\t\t/* backup fp and used registers*/\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSH(SCM_ARG(1)); // get cdr ptr\n"
		"\t\tPUSH(SCM_ARG(0)); // get car ptr\n"
		"\t\tCALL(MAKE_SOB_PAIR);\n"
		"\t\tDROP(2)\n"
		"\t\tPOP(FP); /* restore fp and exit */\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-apply 
	(string-append
		"\tPRIM_SCM_PROC_APPLY:\n"
		"\t\t/* start normal, get parameters as scm args */\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tMOV(R10,SCM_ARG(0)); /* backup target func addr */\n"
		"\t\tMOV(R3,SCM_ARG(1)); /* param lst ptr */\n"
		"\t\t/* reverse param list */\n"
		"\t\tPUSH(R3);\n"
		"\t\tPUSH(IMM(1));\n"
		"\t\tPUSH(INDD(R10,1));\n"
		"\t\tCALL(PRIM_SCM_PROC_REVERSE);\n"
		"\t\tDROP(3);\n"
		"\t\t/* populate stack from reversed list*/\n"
		"\t\tBEGIN_LOCAL_LABELS PUSH_LOOP,DONE_POP;\n"
		"\t\tMOV(R4,0); /* init param counter */\n"
		"\t\tMOV(R5,R0); /* copy lst head ptr to R5 */\n"
		"\t\tPUSH_LOOP:\n"
		"\t\t\tPUSH(INDD(R5,1));\n"
		"\t\t\tMOV(R6,INDD(R5,2));\n"
		"\t\t\tINCR(R4); /* incr argc */\n"
		"\t\t\tCMP(IND(R6),T_NIL); /* next ptr points to nil? */\n"
		"\t\t\tJUMP_EQ(DONE_POP);\n"
		"\t\t\tMOV(R5,R6); \n"
		"\t\t\tJUMP(PUSH_LOOP);\n"
		"\t\tDONE_POP:\n"
		"\t\tPUSH(R4); /* push argc */\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tPUSH(INDD(R10,1)); \n"
		"\t\tPUSH(FPARG(-1)); /* old ret address */\n"
		"\t\tMOV(R1,R4);\n"
		"\t\tINCR(R1);\n"
		"\t\tMOV(R3,IMM(0)); /* init counter */\n"
		"\t\tMOV(R5,R4); /* iteration counter */\n"
		"\t\tADD(R5,3); /* also shift ret addr, env, argc */\n"
		"\t\tMOV(FP,FPARG(-2)); /* restore FP and shift stack*/\n"
		"\t\tMOV(R6,IMM(6)); /* offset - [stored fp ret_addr env_ptr _argc _apply-func list-ptr] */\n"
		"\t\tBEGIN_LOCAL_LABELS SHIFT_STACK;\n"
		"\t\t\tSHIFT_STACK:\n"
		"\t\t\tMOV(R2,R1); \n"
		"\t\t\tADD(R2,R6);\n"
		"\t\t\tMOV(STARG(R2),STARG(R1));\n"
		"\t\t\tMOV(STARG(R1),IMM(0)); \n"
		"\t\t\tDECR(R1);\n"
		"\t\t\tINCR(R3);\n"
		"\t\t\tCMP(R3,R5);\n"
		"\t\t\tJUMP_LE(SHIFT_STACK);\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tSUB(SP,R6);\n"
		"\t\tJUMPA(INDD(R10,2));\n"
))

(define prim-scm-proc-lst
	(string-append
		"\t/* takes a list of arguments and makes nested pairs outta them */\n"
		"\tPRIM_SCM_PROC_LIST:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tBEGIN_LOCAL_LABELS PAIR_LOOP;\n"
		"\t\tMOV(R2,SCM_ARGC); /* init counter */\n"
		"\t\tDECR(R2);\n"
		"\t\t/* set null as cdr for first pair */\n"
		"\t\tMOV(R0,IMM(SOB_NIL));\n"
		"\t\tPAIR_LOOP:\n"
		"\t\t\tPUSH(R0);\n"
		"\t\t\tPUSH(SCM_ARG(R2));\n"
		"\t\t\tCALL(MAKE_SOB_PAIR);\n"
		"\t\t\tDROP(2);\n"
		"\t\t\tDECR(R2);\n"
		"\t\t\tCMP(R2,0);\n"
		"\t\t\tJUMP_GE(PAIR_LOOP);\n"
		"\t\tEND_LOCAL_LABELS \n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-reverse
	(string-append
		"\t/* reverses a given list */\n"
		"\tPRIM_SCM_PROC_REVERSE:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1,SCM_ARG(0)); /* first link ptr */\n"
		"\t\tMOV(R2,R1); /* current link ptr */\n"
		"\t\tBEGIN_LOCAL_LABELS REV_LOOP,EXIT;\n"
		"\t\tREV_LOOP:\n"
		"\t\t\tMOV(R3,INDD(R2,2)); /* next link ptr */\n"
		"\t\t\tMOV(INDD(R2,2),R1);\n"
		"\t\t\tMOV(R1,R2);\n"
		"\t\t\tMOV(R2,R3);\n"
		"\t\t\tCMP(IND(R2),T_NIL); /* check if obj type is null */\n"
		"\t\t\tJUMP_EQ(EXIT);\n"
		"\t\t\tJUMP(REV_LOOP);\n"
		"\t\tEXIT:\n"
		"\t\t/* set last ptr to null*/\n"
		"\t\tMOV(R3,SCM_ARG(0));\n"
		"\t\tMOV(INDD(R3,2),R2);\n"
		"\t\t/* return first link ptr as list head ptr */\n"
		"\t\tMOV(R0,R1);\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
	))

; ***** string and symbol functions

(define prim-scm-proc-string_to_symbol 
	(string-append
		"\tPRIM_SCM_PROC_STRING_TO_SYMBOL:\n"
		"\t\t/* backup fp and used registers*/\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1, SCM_ARG(0)); /* move string ptr R1 */\n"
		"\t\tMOV(R2, STRINGS_LIST_ANCHOR); ; /* get string name anchor ptr */\n"
		"\t\tBEGIN_LOCAL_LABELS CMP_LOOP,ADD_LINK,MAKE_SYMBOL;\n"
		"\t\tCMP_LOOP:\n"
		"\t\t\tPUSH(INDD(R2,1));\n"
		"\t\t\tPUSH(R1);\n"
		"\t\t\tPUSH(2);\n"
		"\t\t\tPUSH(SCM_ENV);\n"
		"\t\t\tCALL(PRIM_SCM_PROC_STRING_EQUAL);\n"
		"\t\t\tDROP(4);\n"
		"\t\t\tCMP(R0,IMM(SOB_TRUE));\n"
		"\t\t\tJUMP_EQ(MAKE_SYMBOL);\n"
		"\t\t\tCMP(IND(R2),IMM(T_NIL)); /* check if next link ptr is null */\n"
		"\t\t\tJUMP_EQ(ADD_LINK);\n"
		"\t\t\tMOV(R2,IND(R2));\n"
		"\t\t\tJUMP(CMP_LOOP);\n"
		"\t\tADD_LINK:\n"
		"\t\t\tPUSH(IMM(2));\n"
		"\t\t\tCALL(MALLOC); /* allocate memory for new link */\n"
		"\t\t\tDROP(1);\n"
		"\t\t\tMOV(IND(R0),IMM(T_NIL)); /* set next ptr to null */\n"
		"\t\t\tMOV(INDD(R0,1),R1); /* set str ptr */\n"
		"\t\t\tMOV(IND(R2),R0);\n"
		"\t\t\tMOV(R2,R0); /* set name ptr to new link */\n"
		"\t\tMAKE_SYMBOL:\n"
		"\t\t\tPUSH(IMM(2)); /* allocate memory for new symbol */\n"
		"\t\t\tCALL(MALLOC);\n"
		"\t\t\tDROP(1);\n"
		"\t\t\tMOV(IND(R0),IMM(T_SYMBOL));\n"
		"\t\t\tMOV(INDD(R0,1),INDD(R2,1));\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP); /* restore fp and exit */\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-symbol_to_string 
	(string-append
		"\tPRIM_SCM_PROC_SYMBOL_TO_STRING:\n"
		"\t\t/* backup fp and used registers*/\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1, SCM_ARG(0)); /* move symbol ptr R1 */\n"
		"\t\tMOV(R0, INDD(R1,1)); /* get string ptr from symbol */\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP); /* restore fp and exit */\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-not
	(string-append
		"\tPRIM_SCM_PROC_NOT:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tBEGIN_LOCAL_LABELS SET_TRUE, EXIT; \n"
		"\t\tMOV(R1,SCM_ARG(0));\n"
		"\t\tCMP(R1,IMM(SOB_FALSE));\n"
		"\t\tJUMP_EQ(SET_TRUE);\n"
		"\t\tMOV(R0,IMM(SOB_FALSE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tSET_TRUE:\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tEXIT:\n"
		"\t\tPOP(FP);\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-char-equal
	(string-append
		"\tPRIM_SCM_PROC_CHAR_EQUAL:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tBEGIN_LOCAL_LABELS START_OP, CMP_LOOP, FAIL, EXIT;\n"
		"\t\tCMP(SCM_ARGC,IMM(1));\n"
		"\t\tJUMP_GT(START_OP);\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tSTART_OP:\n"
		"\t\tMOV(R1,IMM(1));\n"
		"\t\tMOV(R2,SCM_ARG(0)); /* indd(scm_arg,offset) is possible but I condsider it bad. */\n"
		"\t\tCMP_LOOP:\n"
		"\t\t\tMOV(R3,SCM_ARG(R1));\n"
		"\t\t\tCMP(INDD(R2,1),INDD(R3,1));\n"
		"\t\t\tJUMP_NE(FAIL);\n"
		"\t\t\tINCR(R1);\n"
		"\t\t\tCMP(R1,SCM_ARGC);\n"
		"\t\t\tJUMP_LT(CMP_LOOP);\n"
		"\t\tMOV(R0,IMM(SOB_TRUE));\n"
		"\t\tJUMP(EXIT);\n"
		"\t\tFAIL:\n"
		"\t\tMOV(R0,IMM(SOB_FALSE));\n"
		"\t\tEXIT:\n"
		"\t\tPOPAD;\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
	))

(define prim-scm-proc-vector-to-list
	(string-append
		"\tPRIM_SCM_PROC_VECTOR_TO_LIST:\n"
		"\t\tPUSH(FP);\n"
		"\t\tMOV(FP,SP);\n"
		"\t\tPUSHAD;\n"
		"\t\tMOV(R1,SCM_ARG(0));\n"
		"\t\tMOV(R2,0);\n"
		"\t\tMOV(R3,IMM(SOB_NIL));\n"
		"\t\tBEGIN_LOCAL_LABELS LOOP;\n"
		"\t\tLOOP:\n"
		"\t\t\tPUSH(IMM(3));\n"
		"\t\t\tCALL(MALLOC);\n"
		"\t\t\tDROP(1);\n"
		"\t\t\tMOV(IND(R0),IMM(T_PAIR));\n"
		"\t\t\tMOV(INDD(R0,1),INDD(R1,2+R2));\n"
		"\t\t\tMOV(INDD(R0,2),R3);\n"
		"\t\t\tMOV(R3,R0);\n"
		"\t\t\tINCR(R2);\n"
		"\t\t\tCMP(R2,INDD(R1,1));\n"
		"\t\t\tJUMP_LT(LOOP);\n"
		"\t\tEND_LOCAL_LABELS\n"
		"\t\tPOPAD;\n"
		"\t\tPOP(FP);\n"
		"\t\tRETURN;\n"
	))
; ------------------------------ factory callers ---------------------------------------------------------------

; ***** relation functions *****
(define prim-scm-proc-equiv (make-prim-scm-proc-rel "PRIM_SCM_PROC_EQUIV" "JUMP_NE")) 
(define prim-scm-proc-gt (make-prim-scm-proc-rel "PRIM_SCM_PROC_GT" "JUMP_LE")) 
(define prim-scm-proc-lt (make-prim-scm-proc-rel "PRIM_SCM_PROC_LT" "JUMP_GE")) 

; ***** arithmetic functions *****
(define prim-scm-proc-add  (make-prim-scm-proc-arithmetic "PRIM_SCM_PROC_ADD" "ADD" 0)) 
(define prim-scm-proc-sub (make-prim-scm-proc-arithmetic "PRIM_SCM_PROC_SUB" "SUB" 0)) 
(define prim-scm-proc-mul (make-prim-scm-proc-arithmetic "PRIM_SCM_PROC_MUL" "MUL" 1)) 
(define prim-scm-proc-div (make-prim-scm-proc-arithmetic "PRIM_SCM_PROC_DIV" "DIV" 1)) 

; ***** type? functions *****
(define prim-scm-proc-is-boolean (make-prim-scm-proc-is "BOOLEAN" "T_BOOL")) 
(define prim-scm-proc-is-char (make-prim-scm-proc-is "CHAR" "T_CHAR"))	 
(define prim-scm-proc-is-integer (make-prim-scm-proc-is "INTEGER" "T_INTEGER")) 
(define prim-scm-proc-is-null (make-prim-scm-proc-is "NULL" "T_NIL")) 
(define prim-scm-proc-is-pair (make-prim-scm-proc-is "PAIR" "T_PAIR")) 
(define prim-scm-proc-is-procedure (make-prim-scm-proc-is "PROCEDURE" "T_CLOSURE")) 
(define prim-scm-proc-is-string (make-prim-scm-proc-is "STRING" "T_STRING")) 
(define prim-scm-proc-is-symbol (make-prim-scm-proc-is "SYMBOL" "T_SYMBOL")) 
(define prim-scm-proc-is-vector (make-prim-scm-proc-is "VECTOR" "T_VECTOR")) 

; ***** type1->type2 functions *****

(define prim-scm-proc-char_to_integer (make-prim-scm-proc-convert-char-int "CHAR" "INTEGER")) 
(define prim-scm-proc-integer_to_char (make-prim-scm-proc-convert-char-int "INTEGER" "CHAR")) 

; ***** pair functions *****
(define prim-scm-proc-car (make-prim-scm-proc-pair-get "CAR" "1")) 
(define prim-scm-proc-set-car (make-prim-scm-proc-pair-set "CAR" "1")) 
(define prim-scm-proc-cdr (make-prim-scm-proc-pair-get "CDR" "2")) 
(define prim-scm-proc-set-cdr (make-prim-scm-proc-pair-set "CDR" "2"))

; ***** string functions *****
(define prim-scm-proc-make-string (make-prim-scm-proc-vtypes "STRING" #t))
(define prim-scm-proc-string-set (make-prim-scm-proc-vtypes-set "STRING" "\t\tMOV(R3,INDD(R3,1));\t// override ptr with actual value\n"))
(define prim-scm-proc-string-ref (make-prim-scm-proc-vtypes-ref "STRING" "\t\tCALL(MAKE_SOB_CHAR);\n\t\tDROP(1);\t\t\n" "\t\tPUSH(IMM(0)); // push false\n"))
(define prim-scm-proc-string-length (make-prim-scm-proc-vtypes-length "STRING"))


; ***** vector functions *****
(define prim-scm-proc-make-vector (make-prim-scm-proc-vtypes "VECTOR" #f))
(define prim-scm-proc-vector-set (make-prim-scm-proc-vtypes-set "VECTOR" "\t\tNOP;\n"))
(define prim-scm-proc-vector-ref (make-prim-scm-proc-vtypes-ref "VECTOR" "\t\tPOP(R0);" "\t\tMOV(R0,IMM(SOB_NIL));\n"))
(define prim-scm-proc-vector-length (make-prim-scm-proc-vtypes-length "VECTOR"))


					

					

