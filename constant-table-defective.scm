; Defines:

(define private-const-lst `())
(define extended-const-lst `())
(define last-allocated-addr 0);
(define make-void (if #f #f))

(define void?
	(lambda (_expr)
		(eq? _expr (if #f #f))))

; Makes:

(define make-const-table-entry
	(lambda (_const-lst _index)
		(cond
			((null? _const-lst) `())
			(else (let ((_entry (car _const-lst)))
					`(
					,(string-append "\t\tMOV(IND(" (number->string _index) "), IMM(" (if (symbol? _entry) (formatter (string-upcase(symbol->string _entry))) (format "~s" _entry))"));\n")
					,@(make-const-table-entry (cdr _const-lst) (+ 1 _index))
					)
				)
			)
		)))

(define add-consts-to-extended-list
	(lambda (_expr)
		(let ((_const-info (get-const-info _expr)))
			(begin 
				(set! extended-const-lst (append extended-const-lst `(,_const-info)))
				(set! last-allocated-addr (+ last-allocated-addr (length (caddr _const-info))))
			)
		)
	))



(define make-cisc-const-table
	(lambda (_extended-const-lst)
		(let ((_flat-init-list (flatten (map (lambda (_elem) (caddr _elem)) _extended-const-lst))))
			(apply string-append (make-const-table-entry _flat-init-list (caar _extended-const-lst)))
			)))

(define make-cisc-const-macros
	(lambda (_extended-const-lst)
		(let (
			(_scm-void (car _extended-const-lst))
			(_scm-nil (cadr _extended-const-lst))
			(_scm-f (caddr _extended-const-lst))
			(_scm-t (cadddr _extended-const-lst))
			)
		(string-append
			"\t\t#define SOB_VOID " (number->string (car _scm-void)) "\n"
			"\t\t#define SOB_NIL " (number->string (car _scm-nil)) "\n"
			"\t\t#define SOB_FALSE " (number->string (car _scm-f)) "\n"
			"\t\t#define SOB_TRUE " (number->string (car _scm-t)) "\n"
		))))

(define make-extended-const-list
	(lambda (_base-const-lst _base-addr)
			(begin
				(set! extended-const-lst `(
										(,_base-addr ,make-void (T_VOID))
										(,(+ _base-addr 1) ,`() (T_NIL))
										(,(+ _base-addr 2) #f (T_BOOL ,0))
										(,(+ _base-addr 3) #t (T_BOOL ,1))
									))
				(set! last-allocated-addr (+ _base-addr 5))
				(if (> (length _base-const-lst) 0) (map (lambda (_expr) (add-consts-to-extended-list _expr)) (reverse _base-const-lst))) ; VERIFY THIS! map starts from end of list
				extended-const-lst
			)))


; Helping methods:

(define list->set
	(lambda (_lst)
		(if (> (length _lst) 0) ; handle empty lists to always generate base const list
			(let ((_head (car _lst))
					(_tail (cdr _lst)))
			(cond
				((eq? _tail '()) _lst)
				((not (member _head _tail)) (cons _head (list->set _tail)))
				(else (list->set _tail))
				)
			)
			`())
		))

(define get-const-list
	(lambda (_expr)
		(begin
			(set! private-const-lst `())
			(const-traverse-expr-list _expr)
			private-const-lst)))

(define get-const-ptr
	(lambda (_const-val)
		(get-const-ptr-from-lst _const-val extended-const-lst)))

(define get-const-ptr-from-lst
	(lambda (_const-val _const-lst)
		(let ((_const-info (car _const-lst)))
			(if (equal? _const-val (cadr _const-info)) (car _const-info) (get-const-ptr-from-lst _const-val (cdr _const-lst))))))

(define get-const-info
	(lambda (_const)
		(let ((_const-type-size (get-const-c-rep _const)))
			`(,(+ last-allocated-addr 1) ,_const ,_const-type-size))))		

(define get-const-c-rep ; return C representation of the const
	(lambda (_const)
		(cond
		((integer? _const) `(T_INTEGER ,_const))
		((char? _const) `(T_CHAR ,(char->integer _const)))
		((string? _const) `(T_STRING ,(string-length _const) ,@(map (lambda (_char) (char->integer _char)) (string->list _const))))
		((vector? _const) `(T_VECTOR ,(vector-length _const) ,@(map (lambda (_elem) (get-const-ptr _elem)) (vector->list _const))))
		((list? _const) `(T_PAIR ,(get-const-ptr (car _const)) ,(get-const-ptr (cdr _const))))
		((symbol? _const) `(T_SYMBOL ,(get-const-ptr (symbol->string _const))))
		((pair? _const) `(T_PAIR ,(get-const-ptr (car _const)) ,(get-const-ptr (cdr _const)))) 
		)))

; Main functions:

(define const-topological-sort
    (lambda (_expr)
        (cond 
        	  ((or (void? _expr) (null? _expr) (boolean? _expr)) `())
              ((or (number? _expr) (char? _expr) (string? _expr)) `(,_expr))
              ((symbol? _expr) `(,@(const-topological-sort (symbol->string _expr)) ,_expr))
              ((pair? _expr) `(,@(const-topological-sort (car _expr)) ,@(const-topological-sort (cdr _expr)) ,_expr))
              ((vector? _expr) `(,@(apply append (map const-topological-sort (vector->list _expr))) ,_expr))
              (else (error "constant topological sorting" "unknown type")))))



(define const-traverse-expr-list
	(lambda (_expr)
		(cond
			((is? _expr 'const) (set! private-const-lst (append private-const-lst (list (cdr _expr)))))
			((is? _expr 'lexical-var) `())
			((is? _expr 'lambda-opt) `(,@(const-traverse-expr-list (cadddr _expr))))
			((is? _expr 'if) 
				`(
					,@(const-traverse-expr-list (cadr _expr))
					,@(const-traverse-expr-list (caddr _expr))
					,@(const-traverse-expr-list (cadddr _expr))
					))
			((or (is? _expr 'define) (is? _expr 'lambda-simple) (is? _expr 'lambda-variadic))
				`(,@(const-traverse-expr-list (caddr _expr))))
			((or (is? _expr 'or) (is? _expr 'seq)) `(,@(const-traverse-expr-list (cadr _expr))))
			(else 
				(if (is-last-element? _expr) ; if cdr is null, check if expr is null. if it isn't, it's a nested list so check car
					(if (null? _expr) _expr (const-traverse-expr-list (car _expr)))
				`(,(const-traverse-expr-list (cadr _expr)) , ; cdr isn't null, so assume it's applic or matching form
				(map (lambda (_exp) (const-traverse-expr-list _exp)) (caddr _expr)))
				)
			)
		)))	
