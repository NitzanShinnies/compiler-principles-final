(define make-indentation 
    (lambda (_count)
      (if (= 0 _count) "" (string-append "\t" (make-indentation (- _count 1))))))

(define code-gen
	(lambda(_expr _env-size _argc _indent-index)
		(cond
			((is? _expr 'const) (code-gen-const _expr (make-indentation _indent-index)))
			((is? _expr 'fvar) (code-gen-free-var _expr (make-indentation _indent-index)))
			((is? _expr 'pvar) (code-gen-pvar _expr (make-indentation _indent-index)))
			((is? _expr 'bvar) (code-gen-bvar _expr (make-indentation _indent-index)))
			((is? _expr 'if3) (code-gen-if3 _expr _env-size _argc _indent-index))
			((is? _expr 'seq) (code-gen-seq _expr _env-size _argc _indent-index))
			((is? _expr 'or) (code-gen-or _expr _env-size _argc _indent-index))
			((is? _expr 'define) (code-gen-define _expr _env-size _argc _indent-index))
			((is? _expr 'applic) (code-gen-applic _expr _env-size _argc _indent-index))
			((is? _expr 'tc-applic) (code-gen-tc-applic _expr _env-size _argc _indent-index)) ; TODO: change to TC applic!
			((is? _expr 'lambda-simple) (code-gen-lambda-simple _expr _env-size _argc _indent-index))
			((is? _expr 'lambda-opt) (code-gen-lambda-opt _expr _env-size _argc _indent-index))
			((is? _expr 'lambda-variadic) (code-gen-lambda-variadic _expr _env-size _argc _indent-index))
			(else 
				(display
					(error 
						'code-gen 
						(string-append
							(make-indentation _indent-index)
							(format "/* unable to generate code for ~s. */\n" _expr)
							(make-indentation _shift)
							"\n\t/*--- ALL GLORY TO THE HYPNOTOAD! ---*/\n"
							(die 'compile _indent-index)
						))
				target-output))
		)))


(define code-gen-if3
	(lambda (_if3 _env-size _argc _indent-index)
		(let* (
			(_test (cadr _if3))
			(_dit (caddr _if3))
			(_dif (cadddr _if3))
			(_labels (make-labels 'if3))
			(_label-dif (list-ref _labels 0))
			(_label-exit (list-ref _labels 1))
			(_indent-val (make-indentation _indent-index))
			(_int-indent-val (make-indentation (+ _indent-index 1)))
			)
			
			(begin
				(display (string-append _indent-val "/* ============== start code-gen if3 ============== */\n") target-output)
				(code-gen _test _env-size _argc _indent-index)
				(display (
					string-append 
						_indent-val "CMP(R0, IMM(SOB_FALSE));\n"
						_indent-val "JUMP_EQ(" _label-dif ");\n"
					) 
					target-output)
				(code-gen _dit _env-size _argc (+ _indent-index 1))
				(display 
					(string-append 
						_int-indent-val "JUMP(" _label-exit ");\n" 
						_int-indent-val _label-dif ":\n"
					) 
					target-output) 
				(code-gen _dif _env-size _argc (+ _indent-index 1))
				(display 
					(string-append 
						_indent-val _label-exit ":\n" 					
						_indent-val "/* ============== end code-gen if3 ============== */\n\n"
					) 
					target-output)
			)
		)))

(define code-gen-free-var
	(lambda(_fvar _indent-val)
		(display
			(string-append
				_indent-val "/* ============== start code-gen fvar ============== */\n"
				_indent-val "MOV(R0,IND(" (format "~s" (get-fvar-addr (cadr _fvar))) "));\n\n"
				_indent-val "/* ============== end code-gen fvar ============== */\n\n"
			)
			target-output)
	))

(define code-gen-const
	(lambda (_const _indent-val)
		(display
			(string-append 
				_indent-val "/* ============== start code-gen const ============== */\n"
				_indent-val "MOV(R0,IMM(" (format "~s" (get-const-ptr (cadr _const))) "));\n\n"
				_indent-val "/* ============== end code-gen const ============== */\n\n"
			)
			target-output)
	))

(define code-gen-pvar
	(lambda (_pvar _indent-val)
		(display
			(string-append
				_indent-val "/* ============== start code-gen pvar ============== */\n"
				_indent-val "MOV(R0,SCM_ARG(" (number->string (caddr _pvar)) "));\n\n"
				_indent-val "/* ============== end code-gen pvar ============== */\n\n"
			)
			target-output)
	))

(define code-gen-bvar
	(lambda (_bvar _indent-val)
		(let ((major (caddr _bvar))
			  (minor (cadddr _bvar)))
		(display
			(string-append
				_indent-val "/* ============== start code-gen bvar ============== */\n"
				_indent-val "MOV(R0,SCM_ENV);\n"
				_indent-val "MOV(R0,INDD(R0," (number->string major) "));\n"
				_indent-val "MOV(R0,INDD(R0," (number->string minor) "));\n\n"
				_indent-val "/* ============== end code-gen bvar ============== */\n\n"
			)
			target-output)
			)))

(define code-gen-seq
	(lambda (_seq _env-size _argc _indent-index)
		(begin
			(display (string-append (make-indentation _indent-index) "/* ============== start code-gen seq ============== */\n") target-output)
			(map (lambda (_expr) (code-gen _expr _env-size _argc _indent-index)) (reverse (cadr _seq)))
			(display (string-append (make-indentation _indent-index) "/* ============== end code-gen seq ============== */\n\n") target-output)
		)))

(define code-gen-or
	(lambda (_or _env-size _argc _indent-index)
		(let (
				(_labels (make-labels 'or))
				(_indent-val (make-indentation _indent-index))
			)

			(begin
				(display (string-append _indent-val "/* ============== start code-gen or ============== */\n") target-output)
				(map (lambda (_expr) 
							(begin
								(display 
									(string-append 
										_indent-val "/* " (list-ref _labels 1) " test case start --> */\n"
									)
									target-output)
								(code-gen _expr _env-size _argc _indent-index) 
								(display 
									(string-append
										_indent-val "/* <-- " (list-ref _labels 1) " test case end */\n"
										_indent-val "CMP(R0,IMM(SOB_FALSE));\n"
										_indent-val "JUMP_NE(" (car _labels) ");\n\n"
									)
									target-output
								)))
							(reverse (cadr _or)))
				(display 
					(string-append 
						_indent-val (car _labels) ":\n"
						_indent-val "/* ============== end code-gen or ============== */\n\n"
						) 
					target-output)
		))))

(define code-gen-applic
	(lambda (_applic _env-size _argc _indent-index)
		(let (
			(_app (cadr _applic))
			(_params (caddr _applic))
			(_labels (make-labels 'applic))
			(_indent-val (make-indentation _indent-index))
			)
		(begin
			(display 
				(string-append
					_indent-val "/* ============== start code-gen applic ============== */\n"
					_indent-val "/* _param-count=" (number->string (length _params)) "*/\n"
					_indent-val "PUSH(IMM(T_NIL)); /* magic cell */\n" 
				)
				target-output)
			(map 
				(lambda (_expr) 
					(begin 
						(display 
							(string-append
								_indent-val "/* applic param start --> */ \n"
							)
							target-output) 
						(code-gen _expr _env-size _argc _indent-index) 
						(display 
							(string-append
								_indent-val "PUSH(R0);\n"
								_indent-val "/* <-- applic param enc */\n" 
							)
							target-output)
					))
				 _params)
			
			(display 
				(string-append 
					_indent-val "PUSH(IMM(" (number->string (length _params)) ")); /* applic argc */\n"
					_indent-val "/* applic target start --> */\n"
					) 
				target-output)
			(code-gen _app _env-size _argc _indent-index )
			(display
				(string-append
					_indent-val "/* <-- applic target end */\n"
					_indent-val "CMP(IND(R0),IMM(T_CLOSURE));\n"
					_indent-val "JUMP_EQ(" (car _labels) ");\n"
					_indent-val (die 'closure _indent-index)
					_indent-val (car _labels)":\n"
					_indent-val "PUSH(INDD(R0,1)); /* APPLIC ENV (from closure) */\n"
					_indent-val "CALLA(INDD(R0,2));\n"
					_indent-val "DROP(3+STARG(0));\n\n" 
					_indent-val "/* ============== end code-gen applic ============== */\n\n"
				)
				target-output)
		)
	)))

(define code-gen-tc-applic
	(lambda (_tc-applic _env-size _argc _indent-index)
		(let (
			(_app (cadr _tc-applic))
			(_params (caddr _tc-applic))
			(_labels (make-labels 'tc-applic))
			(_indent-val (make-indentation _indent-index))
			)
		(begin
			(display 
				(string-append
					_indent-val "/* ============== start code-gen tc-applic ============== */\n"
					_indent-val "/* _param-count=" (number->string (length _params)) "*/\n"
					_indent-val "PUSH(IMM(T_NIL)); /* magic cell */\n" 
				)
				target-output)
			(map 
				(lambda (_expr) 
					(begin 
						(display 
							(string-append
								_indent-val "/* tc-applic param start --> */ \n"
							)
							target-output) 
						(code-gen _expr _env-size _argc _indent-index) 
						(display 
							(string-append
								_indent-val "PUSH(R0);\n"
								_indent-val "/* <-- tc-applic param enc */\n" 
							)
							target-output)
					))
				 _params)
			
			(display 
				(string-append 
					_indent-val "PUSH(IMM(" (number->string (length _params)) ")); /* tc-applic argc */\n"
					_indent-val "/* tc-applic target start --> */\n"
					) 
				target-output)
			(code-gen _app _env-size _argc _indent-index)
			(display
				(string-append
					_indent-val "/* <-- tc-applic target end */\n"
					_indent-val "CMP(IND(R0),IMM(T_CLOSURE));\n"
					_indent-val "JUMP_EQ(" (car _labels) ");\n"
					_indent-val (die 'closure _indent-index)
					_indent-val (car _labels)":\n"
					_indent-val "PUSH(INDD(R0,1)); /* TC-APPLIC ENV (from closure) */\n"
					; stack repair code start
					_indent-val "PUSH(FPARG(-1)); /* push return addr */\n"
					_indent-val "MOV(R5,IMM(" (number->string (length _params)) ")); /* tc-applic argc + env + ret + whatever */\n" ; ret_add + env-ptr + argc + applic argc + nil
					_indent-val "ADD(R5,IMM(4));\n"
					_indent-val "MOV(R6,FPARG(1)); /* stack offset modifier */\n"
					_indent-val "ADD(R6,IMM(5));; /* compensate for ret + env + argc + magic + old fp in stack */\n"
					_indent-val "MOV(R7,FPARG(-2)); /* store old fp */\n" 
					_indent-val "MOV(R8,IMM(-3)); /* source ptr */\n"
					_indent-val "MOV(R9,FPARG(1)); /* target ptr */\n"
					_indent-val "ADD(R9,IMM(2));\n"
					_indent-val "BEGIN_LOCAL_LABELS SHIFT_STACK;\n"
					_indent-val "SHIFT_STACK:\n"
					_indent-val "\tMOV(FPARG(R9),FPARG(R8))\n"
					_indent-val "\tDECR(R8);\n"
					_indent-val "\tDECR(R9);\n"
					_indent-val "\tDECR(R5);\n"
					_indent-val "\tCMP(R5,IMM(0));\n"
					_indent-val "JUMP_GT(SHIFT_STACK);\n"
					_indent-val "END_LOCAL_LABELS\n"
					_indent-val "DROP(R6);\n"
					_indent-val "MOV(FP,R7);\n"
					_indent-val "JUMPA(INDD(R0,2));\n"
					_indent-val "/* ============== end code-gen tc-applic ============== */\n\n"
				)
				target-output)
			)
	)))

(define code-gen-define 
	(lambda (_define _env-size _argc _indent-index)
		(let (
				(_defined-expr (cadr _define))
				(_defined-var (cadadr _define))
				(_define-body (caddr _define))
				(_indent-val (make-indentation _indent-index))
			)
			(begin
				(display 
					(string-append 
						_indent-val "/* ============== start code-gen define ============== */\n"
						_indent-val "/* define body start --> */\n"
					) 
					target-output)
				(code-gen _define-body _env-size _argc (+ _indent-index 1))
				(display
					(string-append
						_indent-val "/* <-- define body end */\n"
						_indent-val "MOV(IND(" (number->string (get-fvar-addr _defined-var)) "),R0);\n"
						_indent-val "MOV(R0,IMM(SOB_VOID));\n"
						_indent-val "/* ============== end code-gen define ============== */\n\n"
					)
					target-output)
			)
		)))	 		

(define code-gen-make-lambda-header
	(lambda ( _env-size _argc _label-lambda-body _label-lambda-exit _indent-val)
		(string-append
			_indent-val (format "/* _env-size=~s _argc=~s */\n" _env-size _argc)
			_indent-val "PUSH(IMM(" (number->string (+ _env-size 1)) "));\n"
			_indent-val "CALL(MALLOC);\n"
			_indent-val "DROP(1);\n"
			_indent-val "MOV(R1,R0);\n"
			(if (= _env-size 0)
				""
				(string-append
					_indent-val "/* extending env - _env-size=" (number->string _env-size) " argc=" (number->string _argc) "*/\n"
					_indent-val "MOV(R2,SCM_ENV);\n"
					_indent-val "MOV(R4,IMM(1));\n" ; new env index
					_indent-val "MOV(R5,IMM(0));\n" ; old env index
					_indent-val "BEGIN_LOCAL_LABELS LOOP_COPY_ENV;\n"
					_indent-val "LOOP_COPY_ENV:\n"
					_indent-val "\tMOV(INDD(R1,R4),INDD(R2,R5));\n"
					_indent-val "\tINCR(R4);\n"
					_indent-val "\tINCR(R5);\n"
					_indent-val "\tCMP(R5,IMM(" (number->string _env-size) "));\n"
					_indent-val "\tJUMP_LT(LOOP_COPY_ENV);\n"
					_indent-val "END_LOCAL_LABELS\n"
					_indent-val "/* end extend env */\n"
				))
			(if (= _argc 0)
				(string-append _indent-val "MOV(R3,IMM(T_VOID));\n")
				(string-append
					_indent-val "PUSH(IMM(" (number->string _argc) "));\n"
					_indent-val "CALL(MALLOC);\n"
					_indent-val "DROP(1);\n"
					_indent-val "MOV(R3,R0);\n"
					_indent-val "MOV(R4,IMM(0));\n"
					_indent-val "BEGIN_LOCAL_LABELS LOOP_INSERT_PARAMS;\n"
					_indent-val "LOOP_INSERT_PARAMS:\n"
					_indent-val "\tMOV(INDD(R3,R4),SCM_ARG(R4));\n"
					_indent-val "\tINCR(R4);\n"
					_indent-val "\tCMP(R4,IMM(" (number->string _argc) "));\n"
					_indent-val "\tJUMP_LT(LOOP_INSERT_PARAMS);\n"
					_indent-val "END_LOCAL_LABELS\n"
				))
			_indent-val "MOV(IND(R1),R3);\n"
			_indent-val "PUSH(IMM(3));\n"
			_indent-val "CALL(MALLOC);\n"
			_indent-val "DROP(1);\n"
			_indent-val "MOV(IND(R0),IMM(T_CLOSURE));\n"
			_indent-val "MOV(INDD(R0,1),R1);\n"
			_indent-val "MOV(INDD(R0,2),LABEL(" _label-lambda-body "));\n"
			_indent-val "JUMP(" _label-lambda-exit ");\n"
	)))

(define code-gen-lambda-simple
	(lambda (_lambda-simple _env-size _argc _indent-index)
		(let* (
			(_labels (make-labels 'lambda-simple))
			(_label-lambda-body (list-ref _labels 0))
			(_label-lambda-exit (list-ref _labels 1))
			(_params (cadr _lambda-simple))
			(_body (caddr _lambda-simple))
			(_indent-val (make-indentation _indent-index))
			)
			(begin
				(display 
					(string-append
						_indent-val "/* ============== start code-gen lambda-simple ============== */\n"
						(code-gen-make-lambda-header _env-size _argc _label-lambda-body _label-lambda-exit _indent-val)
						_indent-val _label-lambda-body ":\n"
						_indent-val "PUSH(FP);\n"
						_indent-val "MOV(FP,SP);\n"
					)
					target-output)

				(code-gen _body (+ _env-size 1) (length _params) (+ _indent-index 1))
				(display 
					(string-append
						_indent-val "POP(FP);\n"
						_indent-val "RETURN;\n"
						_indent-val _label-lambda-exit ":\n"
						_indent-val "/* ============== end code-gen lambda-simple ============== */\n\n"
					)
					target-output)
			)
		)))

(define code-gen-lambda-opt
	(lambda (_lambda-opt _env-size _argc _indent-index)
		(let* (
			(_labels (make-labels 'lambda-opt))
			(_label-lambda-body (list-ref _labels 0))
			(_label-lambda-exit (list-ref _labels 1))
			(_params (cadr _lambda-opt))
			(_opt-params (caddr _lambda-opt))
			(_body (cadddr _lambda-opt))
			(_indent-val (make-indentation _indent-index))
			)
	;		(display (format "gen lambda simple env_size=~s argc=~s params=~s length=~s\n" _env-size _argc _params (length _params)))
			(begin
				(display
					(string-append
						_indent-val "/* ============== start code-gen lambda-opt ============== */\n"
						(code-gen-make-lambda-header _env-size _argc _label-lambda-body _label-lambda-exit _indent-val)
						_indent-val _label-lambda-body ":\n"
						_indent-val "PUSH(FP);\n"
						_indent-val "MOV(FP,SP);\n"
						_indent-val "BEGIN_LOCAL_LABELS FIX_STACK , SHIFT_STACK , NO_FIX, NO_SHIFT;\n"
						_indent-val "CMP(SCM_ARGC,IMM(" (number->string (length _params)) "));\n"
						_indent-val "JUMP_EQ(NO_FIX);\n"
						_indent-val "MOV(R1,SCM_ARGC);\n"
						_indent-val "DECR(R1);\n"
						_indent-val "MOV(R2,IMM(SOB_NIL));\n"
						_indent-val "FIX_STACK:\n"
						_indent-val "\tPUSH(IMM(3));\n"
						_indent-val "\tCALL(MALLOC);\n"
						_indent-val "\tDROP(1);\n"
						_indent-val "\tMOV(IND(R0),IMM(T_PAIR));\n"
						_indent-val "\tMOV(INDD(R0,1),SCM_ARG(R1));\n"
						_indent-val "\tMOV(INDD(R0,2),R2);\n"
						_indent-val "\tMOV(R2,R0);\n"
						_indent-val "\tDECR(R1);\n"
						_indent-val "\tCMP(R1,IMM(" (number->string (length _params)) "));\n"
						_indent-val "\tJUMP_GE(FIX_STACK);\n"
						_indent-val "MOV(SCM_ARG(" (number->string (length _params)) "),R0);\n"
						_indent-val "MOV(R6,SCM_ARGC);\n"
						_indent-val "SUB(R6," (number->string (+ (length _params) 1)) "); /* check if shift required */\n"
						_indent-val "MOV(R2,SCM_ARGC);\n"
						_indent-val "MOV(SCM_ARGC,IMM(" (number->string (+ (length _params) 1)) "));\n"
						_indent-val "CMP(R6,IMM(0));\n"
						_indent-val "JUMP_EQ(NO_SHIFT);\n"
						_indent-val "/* shift stack to kill old optional params */\n"
						_indent-val "ADD(R2,IMM(2)); /* target register */\n"
						_indent-val "MOV(R1,SCM_ARGC);\n"
						_indent-val "ADD(R1,IMM(2)); /* source register */\n"
						_indent-val "/* R6 holds the SP fix size, don't touch it! */\n"
						_indent-val "MOV(R5,SP);\n"
						_indent-val "SUB(R5,R1);\n"
						_indent-val "INCR(R5);\n"
						_indent-val "SHIFT_STACK:\n"
						_indent-val "\tMOV(STARG(R2),STARG(R1));\n"
						_indent-val "\tDECR(R1);\n"
						_indent-val "\tDECR(R2);\n"
						_indent-val "\tDECR(R5);\n"
						_indent-val "\tCMP(R5,0);\n"
						_indent-val "JUMP_GE(SHIFT_STACK);\n"
						_indent-val "DROP(R6);\n"
						_indent-val "MOV(FP,SP);\n"
						_indent-val "NO_SHIFT:\n"
						_indent-val "NOP; /* fix stupid label macro error */\n"
						_indent-val "NO_FIX:\n"
						_indent-val "NOP;\n"
						_indent-val "END_LOCAL_LABELS\n"
					)
					target-output)
				(code-gen _body (+ _env-size 1) (+ (length _params) 1) (+ _indent-index 1))
				(display
					(string-append
						_indent-val "POP(FP);\n"
						_indent-val "RETURN;\n"
						_indent-val _label-lambda-exit ":\n"
						_indent-val "/* ============== end code-gen lambda-opt ============== */\n\n"

					)
					target-output)
				)
		)))

(define code-gen-lambda-variadic
	(lambda (_lambda-var _env-size _argc _indent-index)
		(let* (
			(_labels (make-labels 'lambda-variadic))
			(_label-lambda-body (list-ref _labels 0))
			(_label-lambda-exit (list-ref _labels 1))
			(_opt-params (cadr _lambda-var))
			(_body (caddr _lambda-var))
			(_indent-val (make-indentation _indent-index))
			)
			(begin
				(display
					(string-append
						_indent-val "/* ============== start code-gen lambda-variadic ============== */\n"
						(code-gen-make-lambda-header _env-size _argc _label-lambda-body _label-lambda-exit _indent-val)
						_label-lambda-body ":\n"
						_indent-val "PUSH(FP);\n"
						_indent-val "MOV(FP,SP);\n"
						_indent-val "BEGIN_LOCAL_LABELS FIX_STACK , SHIFT_STACK , NO_FIX, NO_SHIFT;\n"
						_indent-val "CMP(SCM_ARGC,IMM(0));\n"
						_indent-val "JUMP_EQ(NO_FIX);\n"
						_indent-val "MOV(R1,SCM_ARGC);\n"
						_indent-val "DECR(R1);\n"
						_indent-val "MOV(R2,IMM(SOB_NIL));\n"
						_indent-val "FIX_STACK:\n"
						_indent-val "\tPUSH(IMM(3));\n"
						_indent-val "\tCALL(MALLOC);\n"
						_indent-val "\tDROP(1);\n"
						_indent-val "\tMOV(IND(R0),IMM(T_PAIR));\n"
						_indent-val "\tMOV(INDD(R0,1),SCM_ARG(R1));\n"
						_indent-val "\tMOV(INDD(R0,2),R2);\n"
						_indent-val "\tMOV(R2,R0);\n"
						_indent-val "\tDECR(R1);\n"
						_indent-val "\tCMP(R1,IMM(0));\n"
						_indent-val "\tJUMP_GE(FIX_STACK);\n"
						_indent-val "MOV(SCM_ARG(0),R0);\n"
						_indent-val "MOV(R6,SCM_ARGC);\n"
						_indent-val "MOV(R2,SCM_ARGC);\n"
						_indent-val "MOV(SCM_ARGC,IMM(1));\n"
						_indent-val "DECR(R6); /* shift offset */\n"
						_indent-val "CMP(R6,IMM(0));\n"
						_indent-val "JUMP_EQ(NO_SHIFT);\n"
						_indent-val "/* shift stack to kill old optional params */\n"
						_indent-val "ADD(R2,IMM(2)); /* target register */\n"
						_indent-val "MOV(R1,IMM(3)); /* source register */\n"
						_indent-val "/* R6 holds the SP fix size, don't touch it! */\n"
						_indent-val "MOV(R5,SP);\n"
						_indent-val "SUB(R5,R1);\n"
						_indent-val "INCR(R5);\n"
						_indent-val "SHIFT_STACK:\n"
						_indent-val "\tMOV(STARG(R2),STARG(R1));\n"
						_indent-val "\tDECR(R1);\n"
						_indent-val "\tDECR(R2);\n"
						_indent-val "\tDECR(R5);\n"
						_indent-val "\tCMP(R5,0);\n"
						_indent-val "JUMP_GE(SHIFT_STACK);\n"
						_indent-val "DROP(R6);\n"
						_indent-val "MOV(FP,SP);\n"
						_indent-val "NO_SHIFT:\n"
						_indent-val "NOP; /* fix stupid label macro error */\n"
						_indent-val "NO_FIX:\n"
						_indent-val "NOP;\n"
						_indent-val "END_LOCAL_LABELS\n"
					)
					target-output)
				(code-gen _body (+ _env-size 1) 1 (+ _indent-index 1))
				(display
					(string-append		
						_indent-val "POP(FP);\n"
						_indent-val "RETURN;\n"
						_indent-val _label-lambda-exit ":\n"
						_indent-val "/* ============== end code-gen lambda-variadic ============== */\n\n"
					)
					target-output)
			)
		)))



(define die 
	(lambda (_err-type _indent-index)
		(let (
			(_err-id (assoc _err-type err-search-lst))
			(_indent-val (make-indentation _indent-index))
			)
			(if (eq? _err-id #f) 
				(error 'die "DIE: invalid error type!") ; fail. 
				(string-append
					"\n"
					_indent-val "PUSH(IMM(" (cdr _err-id) "));\n"
					_indent-val "CALL(WRITELN);\n"
					_indent-val "HALT;\n\n"
				)))))